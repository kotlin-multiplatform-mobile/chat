import UIKit
import SwiftUI
import example_shared

struct ComposeViewControllerToSwiftUI: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> UIViewController {
        return MainIosKt.ChatViewController()
    }

    func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
    }
}
