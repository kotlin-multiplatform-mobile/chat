//
//  AppDelegate.swift
//  example_ios
//
//  Created by Hamzah Tossaro on 30/05/24.
//

import SwiftUI
import example_shared
import FirebaseCore

@main
struct ExampleIOS: App {
    init() {
        FirebaseApp.configure()
        ChatKoinKt.doInitKoin(
            apiHost: "identitytoolkit.googleapis.com",
            deviceId: UIDevice.current.identifierForVendor?.uuidString ?? "ios-${random()}",
            appVersion: "1.0.0",
            coreConfig: CoreConfigImpl(),
            authConfig: AuthConfigImpl(),
            chatConfig: ChatConfigImpl()
        )
    }
    
    var body: some Scene {
        WindowGroup {
            ComposeViewControllerToSwiftUI()
                .ignoresSafeArea(edges: .all)
                .ignoresSafeArea(.keyboard)
        }
    }
}
