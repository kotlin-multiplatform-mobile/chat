//
//  CoreConfigImpl.swift
//  example_ios
//
//  Created by hamzah_tossaro on 30/05/24.
//

import example_shared

class CoreConfigImpl: Core_sharedCoreConfig {
    var apiAuthPath: String? = "/v1/accounts:signInWithPassword?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY"
    var apiChannel: String? = "mobile"
    var apiRefreshTokenPath: String? = "/v1/token?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY"
    var headerChannel: String? = "x-header-channel"
    var headerDeviceId: String? = "x-header-device-id"
    var headerLanguage: String? = "x-header-language"
    var headerOs: String? = "x-header-version"
    var headerVersion: String? = "x-header-version"
}
