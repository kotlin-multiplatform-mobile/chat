//
//  AuthConfigImpl.swift
//  example_ios
//
//  Created by hamzah_tossaro on 30/05/24.
//

import example_shared

class AuthConfigImpl: Auth_sharedAuthConfig {
    
    var countryCode: String = "+62"
    var fbAppId: String = "687862339971805"
    var forgetPasswordApi: String = "/coming-soon"
    var googleWebClientId: String = "307690991958-90n86jlhpb13jud4gk71i1prhjle0f73.apps.googleusercontent.com"
    var googleWebClientSecret: String = "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"
    var headerTransactionIdKey: String = "x-header-transaction-id"
    
    var logo: String = "https://cdn-icons-png.flaticon.com/512/6386/6386852.png"
    var onesignalAppId: String = "ea0111d4-7ce7-421a-a069-59fc7df67ece"
    var registerWithAvatar: Bool = false
    var registerWithBio: Bool = false
    var registerApi: String = "/v1/accounts:signUp?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY"
    
    var signInByEmailApi: String = "/v1/accounts:signInWithPassword?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY"
    var signInByPhoneApi: String = "/coming-soon"
    var signInByProviderApi: String = "/v1/accounts:signInWithIdp?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY"
    var signInByBiometricApi: String = "/coming-soon"
    var signOutApi: String = ""
    var validatePhoneApi: String = ""
    var verifyOtpApi: String = ""
    
    func routeSignOutDialog() -> String {
        "/dialog/sign-out"
    }
    
    func routeOTPDialog(phone: String) -> String {
        "/dialog/otp/\(phone)"
    }
    
    func routeForgetPasswordDialog() -> String {
        "/dialog/forget-password"
    }
    
    func routeRegisterScreen() -> String {
        "/screen/register"
    }
    
    func routeSignInScreen() -> String {
        "/screen/sign-in"
    }
    
    private func map(_ jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        guard let resp = jsonObject else { return nil }
        let ticket = Auth_sharedTicket(
            timestamp: nil,
            state: nil,
            otp: nil,
            session: Auth_sharedSession(
                username: nil,
                msisdn: nil,
                role: nil,
                id: String(describing: resp["localId"]!).replacingOccurrences(of: "\"", with: ""),
                email: String(describing: resp["email"]!).replacingOccurrences(of: "\"", with: ""),
                displayName: String(describing: resp["displayName"]!).replacingOccurrences(of: "\"", with: ""),
                expired: nil,
                status: nil,
                token: String(describing: resp["idToken"]!).replacingOccurrences(of: "\"", with: ""),
                profilePicUrl: String(describing: resp["photoUrl"]!).replacingOccurrences(of: "\"", with: ""),
                refreshToken: String(describing: resp["refreshToken"]!).replacingOccurrences(of: "\"", with: ""),
                isMsisdnVerified: nil,
                isEmailVerified: nil,
                isAccountVerified: nil,
                permissions: nil
            ),
            transactionId: nil
        )
        print(ticket)
        return ticket
    }
    
    func forgetPasswordMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return nil
    }
    
    func registerMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return map(jsonObject)
    }
    
    func signInMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return map(jsonObject)
    }
    
    func signInByProviderMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return map(jsonObject)
    }
    
    func signOutMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return nil
    }
    
    func validatePhoneMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return nil
    }
    
    func verifyOtpMapper(jsonObject: [String : Kotlinx_serialization_jsonJsonElement]?) -> Auth_sharedTicket? {
        return map(jsonObject)
    }
}
