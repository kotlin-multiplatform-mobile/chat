//
//  ChatConfigImpl.swift
//  example_ios
//
//  Created by hamzah_tossaro on 07/06/24.
//

import example_shared

class ChatConfigImpl: Chat_sharedChatConfig {
    func routeToChatRoom() -> String {
        "/chat/room"
    }
    
    var logo: String = "https://cdn-icons-png.flaticon.com/512/6386/6386852.png"
    var messageColorSelf: Int64 = 0xFFE5FEFB
    var messageColorOther: Int64 = 0xFFFFFFFF
    var messageColorTime: Int64 = 0xFF979797
    var chatGrpcServerUrl: String = "https://firestore.googleapis.com"
    var userGrpcServerUrl: String = "https://grpc-online-users-7cu2vowjea-et.a.run.app"
    var loadMessageFromFirstTimeJoinRoomOnSameDevice: Bool = false
    var headerTransactionIdKey: String = "x-header-transaction-id"
    var refreshTokenApi: String = "https://securetoken.googleapis.com/v1/token?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY"
}
