# Multi Platform Chat | [![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/kotlin-multiplatform-mobile/chat/tree/develop)

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![pipeline status](https://gitlab.com/kotlin-multiplatform-mobile/chat/badges/main/pipeline.svg)](https://gitlab.com/kotlin-multiplatform-mobile/chat/-/commits/main) [![coverage report](https://gitlab.com/kotlin-multiplatform-mobile/chat/badges/main/coverage.svg)](https://gitlab.com/kotlin-multiplatform-mobile/chat/-/commits/main) [![Latest Release](https://gitlab.com/kotlin-multiplatform-mobile/chat/-/badges/release.svg)](https://gitlab.com/kotlin-multiplatform-mobile/chat/-/releases)

## Contents

- [Documentation](https://gitlab.com/tossaro/kotlin-multi-platform-chat/tree/main/docs)
- [Features](#features)
- [Requirements](#requirements)
- [Usage](#usage)
- [Project Structure](#project-structure)
- [Architecture](#architecture)
- [Commands](#commands)

## Features

- Provide Chat Room:
    - List Online users
    - List Messages
    - Send Message
- Powered by KOIN for dependency injection and using jetpack compose with MVI pattern.

## Requirements

1. Minimum Android/SDK Version: 24
2. Deployment Target iOS/SDK Version: 14.1
3. Target Android/SDK Version: 34
4. Compile Android/SDK Version: 34
5. This project is built using Android Studio version 2023.1.1 and Android Gradle 8.2
6. For iOS, please install [COCOAPODS](https://cocoapods.org/)

## Usage

1. Edit settings.gradle in your root folder:

```groovy
dependencyResolutionManagement {
    repositories {
        //...
        maven { url 'https://gitlab.com/api/v4/projects/58403362/packages/maven' }
    }
}
```

2. Last, add 'implementation "multi.platform.chat:chat_shared:${version}"' inside tag
   dependencies { . . . } of build.gradle app

For the high level hierarchy, the project separate into 2 main modules, which are :

### 1. [Chat Shared](https://gitlab.com/kotlin-multiplatform-mobile/chat/tree/main/core_shared)

This module contains shared code that holds the domain and data layers and some part of the
presentation logic ie.shared viewmodels.

### 2. [Example Shared](https://gitlab.com/kotlin-multiplatform-mobile/chat/tree/main/example_shared)

This module contains usage of this shared module.

### 3. [Example Android](https://gitlab.com/kotlin-multiplatform-mobile/chat/tree/main/example_android)

This module contains runnable app for android.

### 4. [Example iOS](https://gitlab.com/kotlin-multiplatform-mobile/chat/tree/main/example_ios)

This module contains runnable app for iOS.

## Project Structure

```plantuml
:chat_shared;
:example_shared;
fork
    :example_android;
fork again
    :example_ios;
end fork
end
```


## Architecture

This project implement Clean Architecture with MVI

![Image Clean architecture](/resources/mvi.jpeg)

## Commands

Here are some useful gradle/adb commands for executing this example:

* ./gradlew clean build - Build the entire project and execute unit tests
* ./gradlew clean sonarqube - Execute sonarqube coverage report
* ./gradlew assembleStoreDebug dokkaGfm - Generate documentation
* ./gradlew lintStoreDebug - Run linter
* ./gradlew spotlessApply - Run apply spotless
* ./gradlew test{flavor}{buildType}UnitTest - Execute unit tests e.g., testStoreDebugUnitTest
* ./gradlew testStoreDebugUnitTest koverXmlReport - Generate coverage report
* ./gradlew assemble{flavor}{buildType} - Create apk/aar file e.g., assembleStoreDebug
* ./gradlew :chat_shared:assembleXCFramework - Generate XCFramework for iOS
* ./gradlew assembleStoreDebug publish - Publish - Publish to repository packages (MAVEN)