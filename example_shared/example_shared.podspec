Pod::Spec.new do |spec|
    spec.name                     = 'example_shared'
    spec.version                  = '1.0.0'
    spec.homepage                 = 'https://gitlab.com/kotlin-multiplatform-mobile/chat'
    spec.source                   = { :http=> ''}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'Example Usage of KMM Chat'
    spec.vendored_frameworks      = 'build/cocoapods/framework/example_shared.framework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.1'
    spec.osx.deployment_target = '14.2.1'
    spec.dependency 'GoogleSignIn', '7.0.0'
    spec.dependency 'Wire'
                
    if !Dir.exist?('build/cocoapods/framework/example_shared.framework') || Dir.empty?('build/cocoapods/framework/example_shared.framework')
        raise "

        Kotlin framework 'example_shared' doesn't exist yet, so a proper Xcode project can't be generated.
        'pod install' should be executed after running ':generateDummyFramework' Gradle task:

            ./gradlew :example_shared:generateDummyFramework

        Alternatively, proper pod installation is performed during Gradle sync in the IDE (if Podfile location is set)"
    end
                
    spec.pod_target_xcconfig = {
        'KOTLIN_PROJECT_PATH' => ':example_shared',
        'PRODUCT_MODULE_NAME' => 'example_shared',
    }
                
    spec.script_phases = [
        {
            :name => 'Build example_shared',
            :execution_position => :before_compile,
            :shell_path => '/bin/sh',
            :script => <<-SCRIPT
                if [ "YES" = "$OVERRIDE_KOTLIN_BUILD_IDE_SUPPORTED" ]; then
                  echo "Skipping Gradle build task invocation due to OVERRIDE_KOTLIN_BUILD_IDE_SUPPORTED environment variable set to \"YES\""
                  exit 0
                fi
                set -ev
                REPO_ROOT="$PODS_TARGET_SRCROOT"
                "$REPO_ROOT/../gradlew" -p "$REPO_ROOT" $KOTLIN_PROJECT_PATH:syncFramework \
                    -Pkotlin.native.cocoapods.platform=$PLATFORM_NAME \
                    -Pkotlin.native.cocoapods.archs="$ARCHS" \
                    -Pkotlin.native.cocoapods.configuration="$CONFIGURATION"
            SCRIPT
        }
    ]
    spec.resources = ['build/compose/cocoapods/compose-resources']
end