package multi.platform.chat.example.shared

import multi.platform.chat.example.shared.app.onboarding.OnBoardingViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

actual fun exampleModule() = module {
    viewModelOf(::OnBoardingViewModel)
}
