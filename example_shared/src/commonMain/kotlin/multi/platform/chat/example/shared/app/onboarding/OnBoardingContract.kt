package multi.platform.chat.example.shared.app.onboarding

import multi.platform.core.shared.app.common.ViewEvent
import multi.platform.core.shared.app.common.ViewSideEffect
import multi.platform.core.shared.app.common.ViewState

class OnBoardingContract {
    sealed class Event : ViewEvent {
        object SignIn : Event()
        object SignOut : Event()
        object EnterChatRoom : Event()
    }

    data class State(
        val isLoading: Boolean,
        val isError: Boolean,
        val token: String?,
        val displayName: String?,
        val signInWithGoogle: Boolean,
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        data class ShowSnackBar(val message: String) : Effect()
        sealed class Navigation : Effect() {
            object ToSignInScreen : Navigation()
            object ShowSignOutDialog : Navigation()
            object ToChatRoomScreen : Navigation()
        }
    }
}
