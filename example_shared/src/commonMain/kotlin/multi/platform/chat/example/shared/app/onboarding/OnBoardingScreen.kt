package multi.platform.chat.example.shared.app.onboarding

import androidx.compose.foundation.Image
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import coil3.compose.AsyncImage
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import multi.platform.auth.shared.app.signin.SignInViewModel
import multi.platform.auth.shared.external.AuthConfig
import multi.platform.auth.shared.external.constants.AuthKey.SIGN_OUT_KEY
import multi.platform.auth.shared.external.utilities.GoogleAuth
import multi.platform.chat.example_shared.generated.resources.Res
import multi.platform.chat.example_shared.generated.resources.bg_dark
import multi.platform.chat.example_shared.generated.resources.bg_light
import multi.platform.chat.example_shared.generated.resources.onboarding_continue
import multi.platform.chat.example_shared.generated.resources.onboarding_signin
import multi.platform.chat.example_shared.generated.resources.onboarding_signout
import multi.platform.chat.example_shared.generated.resources.onboarding_welcome
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.core.shared.app.common.SIDE_EFFECTS_KEY
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.constants.CommonKey.ACCESS_TOKEN_KEY
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.koinInject

@Composable
fun OnBoardingScreen(
    modifier: Modifier = Modifier,
    authConfig: AuthConfig,
    chatConfig: ChatConfig,
    navController: NavController,
    signOut: State<String>,
    token: State<String>,
    error: State<String>,
) {
    val viewModel = koinInject<OnBoardingViewModel>()
    val signInViewModel = koinInject<SignInViewModel>()
    val snackbarHostState = remember { SnackbarHostState() }

    signOut.value.takeIf { it.isNotEmpty() }?.let {
        viewModel.setState { copy(displayName = null, token = null) }
        navController.currentBackStackEntry
            ?.savedStateHandle
            ?.set(SIGN_OUT_KEY, "")
    }

    token.value.takeIf { it.isNotEmpty() }?.let {
        viewModel.setState { copy(token = it) }
        navController.currentBackStackEntry
            ?.savedStateHandle
            ?.set(ACCESS_TOKEN_KEY, "")
    }

    error.value.takeIf { it.isNotEmpty() }?.let {
        viewModel.setEffect { OnBoardingContract.Effect.ShowSnackBar(it) }
        navController.currentBackStackEntry
            ?.savedStateHandle
            ?.set(CommonKey.ERROR_KEY, "")
    }

    LaunchedEffect(SIDE_EFFECTS_KEY) {
        viewModel.effect.onEach { effect ->
            when (effect) {
                is OnBoardingContract.Effect.Navigation.ToSignInScreen -> {
                    navController.navigate(authConfig.routeSignInScreen())
                }
                is OnBoardingContract.Effect.Navigation.ShowSignOutDialog -> {
                    navController.navigate(authConfig.routeSignOutDialog())
                }
                is OnBoardingContract.Effect.Navigation.ToChatRoomScreen -> {
                    navController.navigate(chatConfig.routeToChatRoom())
                }
                is OnBoardingContract.Effect.ShowSnackBar -> snackbarHostState.showSnackbar(effect.message)
            }
        }.collect()
    }

    Scaffold(
        modifier = modifier,
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState) { data ->
                Snackbar(
                    snackbarData = data,
                    containerColor = Color.Red,
                    contentColor = Color.White,
                )
            }
        },
    ) { p ->
        Box {
            Image(
                painterResource(if (isSystemInDarkTheme()) Res.drawable.bg_dark else Res.drawable.bg_light),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop,
            )
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(22.dp),
            ) {
                Column(
                    modifier = Modifier.weight(1f).fillMaxSize(),
                    verticalArrangement = Arrangement.Center,
                ) {
                    AsyncImage(
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally)
                            .size(200.dp),
                        model = chatConfig.logo,
                        contentDescription = null,
                        colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onBackground),
                    )
                    Text(
                        text = stringResource(Res.string.onboarding_welcome),
                        style = MaterialTheme.typography.bodyLarge,
                        modifier = Modifier
                            .align(Alignment.CenterHorizontally)
                            .padding(top = 30.dp, bottom = 100.dp),
                    )
                }

                viewModel.viewState.value.displayName?.let {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween,
                    ) {
                        Button(
                            onClick = { viewModel.setEvent(OnBoardingContract.Event.SignOut) },
                            colors = ButtonDefaults.outlinedButtonColors(),
                        ) {
                            Text(text = stringResource(Res.string.onboarding_signout))
                        }
                        Button(
                            modifier = Modifier.padding(start = 10.dp),
                            onClick = { viewModel.setEvent(OnBoardingContract.Event.EnterChatRoom) },
                        ) {
                            Text(
                                text = stringResource(Res.string.onboarding_continue, it),
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis,
                            )
                        }
                    }
                } ?: run {
                    Button(
                        onClick = {
                            viewModel.setEffect { OnBoardingContract.Effect.Navigation.ToSignInScreen }
                            // viewModel.setState { copy(signInWithGoogle = true) }
                        },
                        modifier = Modifier.align(Alignment.End),
                    ) {
                        Text(text = stringResource(Res.string.onboarding_signin))
                    }
                }
            }

            if (viewModel.viewState.value.signInWithGoogle) {
                val googleAuth = koinInject<GoogleAuth>()
                googleAuth.SignIn(
                    onSignInSuccess = { session ->
                        viewModel.setState { copy(signInWithGoogle = false, isLoading = false) }
                        signInViewModel.signInWithProvider("google.com", session.token.toString(), {
                            viewModel.setEffect { OnBoardingContract.Effect.Navigation.ToChatRoomScreen }
                        }) { viewModel.setEffect { OnBoardingContract.Effect.ShowSnackBar(it?.message.toString()) } }
                    },
                    onSignInFailed = {
                        viewModel.setState { copy(signInWithGoogle = false, isLoading = false) }
                        viewModel.setEffect { OnBoardingContract.Effect.ShowSnackBar(it) }
                    },
                    listOf(),
                )
            }
        }
    }
}
