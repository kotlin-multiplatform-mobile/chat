package multi.platform.chat.example.shared

import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.domain.chat.entity.User
import org.koin.dsl.module

class ExampleModule() {
    operator fun invoke() = module {
        single { Realm.open(realmConfig) }
        includes(exampleModule())
    }

    val realmConfig = RealmConfiguration.Builder(
        schema = setOf(Message::class, User::class),
    )
        .name("chat.realm")
        .schemaVersion(1)
        .deleteRealmIfMigrationNeeded()
        .build()
}
