package multi.platform.chat.example.shared.app.onboarding

import multi.platform.core.shared.app.common.ComposeViewModel
import multi.platform.core.shared.external.constants.CommonKey.ACCESS_TOKEN_KEY
import multi.platform.core.shared.external.constants.CommonKey.DISPLAY_NAME_KEY
import multi.platform.core.shared.external.utilities.Persistent

class OnBoardingViewModel(
    private val persistent: Persistent,
) : ComposeViewModel<OnBoardingContract.Event, OnBoardingContract.State, OnBoardingContract.Effect>() {

    init {
        setState {
            copy(
                token = persistent.getString(ACCESS_TOKEN_KEY, null),
                displayName = persistent.getString(DISPLAY_NAME_KEY, null),
            )
        }
    }

    override fun handleEvents(event: OnBoardingContract.Event) {
        when (event) {
            is OnBoardingContract.Event.SignIn ->
                setEffect { OnBoardingContract.Effect.Navigation.ToSignInScreen }
            is OnBoardingContract.Event.SignOut ->
                setEffect { OnBoardingContract.Effect.Navigation.ShowSignOutDialog }
            is OnBoardingContract.Event.EnterChatRoom ->
                setEffect { OnBoardingContract.Effect.Navigation.ToChatRoomScreen }
        }
    }

    override fun setInitialState() = OnBoardingContract.State(
        isLoading = false,
        isError = false,
        token = null,
        displayName = null,
        signInWithGoogle = false,
    )
}
