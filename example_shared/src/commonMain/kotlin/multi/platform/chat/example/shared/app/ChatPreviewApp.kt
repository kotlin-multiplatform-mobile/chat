package multi.platform.chat.example.shared.app

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ProvideTextStyle
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.unit.sp
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import multi.platform.auth.shared.app.AuthNavigation
import multi.platform.auth.shared.external.AuthConfig
import multi.platform.auth.shared.external.constants.AuthKey
import multi.platform.chat.example.shared.app.onboarding.OnBoardingScreen
import multi.platform.chat.shared.app.ChatNavigation
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.extensions.coreComposable
import org.koin.compose.koinInject

@Composable
fun ChatPreviewApp(
    startDestination: String = "main",
    additionalNavigation: (NavGraphBuilder.(NavHostController) -> Unit)? = null,
) {
    val navController = rememberNavController()
    val authConfig = koinInject<AuthConfig>()
    val chatConfig = koinInject<ChatConfig>()

    ChatPreviewTheme {
        NavHost(
            navController = navController,
            startDestination = startDestination,
        ) {
            navigation(
                startDestination = "/onboarding",
                route = "main",
            ) {
                coreComposable("/onboarding") {
                    val token = it.savedStateHandle.getStateFlow(CommonKey.ACCESS_TOKEN_KEY, "").collectAsState()
                    val signOut = it.savedStateHandle.getStateFlow(AuthKey.SIGN_OUT_KEY, "").collectAsState()
                    val error = it.savedStateHandle.getStateFlow(CommonKey.ERROR_KEY, "").collectAsState()
                    OnBoardingScreen(
                        navController = navController,
                        authConfig = authConfig,
                        chatConfig = chatConfig,
                        signOut = signOut,
                        token = token,
                        error = error,
                    )
                }
            }

            AuthNavigation(
                authConfig = authConfig,
                navController = navController,
                routeName = "auth",
                startDestination = authConfig.routeSignInScreen(),
            )

            ChatNavigation(
                chatConfig = chatConfig,
                navController = navController,
                routeName = "chat",
                startDestination = chatConfig.routeToChatRoom(),
                signOutDestination = authConfig.routeSignOutDialog(),
            )

            if (additionalNavigation != null) {
                this.additionalNavigation(navController)
            }
        }
    }
}

@Composable
fun ChatPreviewTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit,
) {
    MaterialTheme(
        colorScheme = if (darkTheme) darkColorScheme() else lightColorScheme(),
    ) {
        ProvideTextStyle(LocalTextStyle.current.copy(letterSpacing = 0.sp)) {
            content()
        }
    }
}
