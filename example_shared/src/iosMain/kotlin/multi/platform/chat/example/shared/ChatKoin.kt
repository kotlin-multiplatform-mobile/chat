package multi.platform.chat.example.shared

import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import multi.platform.auth.shared.AuthModule
import multi.platform.auth.shared.external.AuthConfig
import multi.platform.chat.shared.ChatModule
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.core.shared.createJson
import multi.platform.core.shared.external.CoreConfig
import multi.platform.core.shared.external.utilities.DefaultPersistentImpl
import multi.platform.core.shared.external.utilities.Persistent
import multi.platform.core.shared.external.utilities.network.ApiClientProvider
import multi.platform.core.shared.external.utilities.network.KtorApiClientImpl
import multi.platform.core.shared.getPlatform
import multi.platform.core.shared.platformModule
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

@Suppress("UnUsed")
fun initKoin(
    apiHost: String,
    deviceId: String,
    appVersion: String,
    coreConfig: CoreConfig,
    authConfig: AuthConfig,
    chatConfig: ChatConfig,
) {
    startKoin {
        modules(
            module {
                single<HttpClientEngine> { getPlatform().apiEngine }
                single<Persistent> { DefaultPersistentImpl() }
                singleOf(::createJson)
                single<CoreConfig> { coreConfig }
                single<ApiClientProvider<HttpClient>> {
                    KtorApiClientImpl(
                        get(),
                        null,
                        null,
                        get(),
                        get(),
                        getOrNull(),
                        apiHost,
                        get(),
                        deviceId,
                        appVersion,
                    )
                }
            },
            platformModule(),
            ExampleModule()(),
            AuthModule(authConfig)(),
            ChatModule(chatConfig)(),
        )
    }
}
