package multi.platform.chat.example.shared

import multi.platform.chat.example.shared.app.onboarding.OnBoardingViewModel
import org.koin.core.module.dsl.factoryOf
import org.koin.dsl.module

actual fun exampleModule() = module {
    factoryOf(::OnBoardingViewModel)
}
