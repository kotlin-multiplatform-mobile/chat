package multi.platform.chat.shared.app

import androidx.compose.ui.window.ComposeUIViewController
import multi.platform.chat.example.shared.app.ChatPreviewApp
import platform.UIKit.UIViewController

fun ChatViewController(): UIViewController = ComposeUIViewController {
    ChatPreviewApp()
}
