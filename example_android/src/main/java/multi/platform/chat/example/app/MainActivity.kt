package multi.platform.chat.example.app

import android.animation.ObjectAnimator
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.annotation.RequiresApi
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import multi.platform.chat.example.shared.app.ChatPreviewApp
import multi.platform.chat.example.shared.app.ChatPreviewTheme
import multi.platform.core.shared.app.common.CoreActivity

class MainActivity : CoreActivity() {
    @RequiresApi(Build.VERSION_CODES.S)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            ChatPreviewApp()
        }
        splashScreen.setOnExitAnimationListener {
            val anim = ObjectAnimator.ofFloat(it, View.ALPHA, 1f, 0f)
            anim.duration = 300L
            anim.start()
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun ThemePreview() {
    ChatPreviewTheme(true) {
        Text("Dark Theme", color = MaterialTheme.colorScheme.primary)
    }
}
