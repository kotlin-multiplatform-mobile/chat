package multi.platform.chat.example

import android.provider.Settings
import com.onesignal.OneSignal
import com.onesignal.debug.LogLevel
import multi.platform.auth.shared.AuthModule
import multi.platform.chat.example.external.AuthConfigImpl
import multi.platform.chat.example.external.ChatConfigImpl
import multi.platform.chat.example.external.CoreConfigImpl
import multi.platform.chat.example.shared.ExampleModule
import multi.platform.chat.shared.ChatModule
import multi.platform.core.shared.app.CoreApplication
import multi.platform.core.shared.external.CoreConfig
import multi.platform.core.shared.external.extensions.toMD5
import org.koin.core.KoinApplication
import org.koin.dsl.module

class ExampleApp : CoreApplication() {
    override fun apiHost() = BuildConfig.AUTH_SERVER
    override fun sharedPrefsName() = "pri0r_m1cr0_h1ght_4uTh"
    override fun appVersion() = getString(R.string.app_version)

    @Suppress("HardwareIds")
    override fun deviceId() =
        Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            .toString().toMD5()

    override val koinApp: (KoinApplication) -> Unit = { app ->
        app.apply {
            modules(
                module {
                    single<CoreConfig> { CoreConfigImpl() }
                },
                ExampleModule()(),
                AuthModule(AuthConfigImpl())(),
                ChatModule(ChatConfigImpl())(),
            )
        }
    }

    override fun onLaunch() {
        super.onLaunch()
        if (BuildConfig.ONESIGNAL_APP_ID.isNotEmpty()) {
            OneSignal.Debug.logLevel = LogLevel.VERBOSE
            OneSignal.initWithContext(this, BuildConfig.ONESIGNAL_APP_ID)
        }
    }
}
