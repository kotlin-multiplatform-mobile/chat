package multi.platform.chat.example.external

import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.boolean
import kotlinx.serialization.json.contentOrNull
import kotlinx.serialization.json.jsonPrimitive
import multi.platform.auth.shared.domain.auth.entity.Session
import multi.platform.auth.shared.domain.auth.entity.Ticket
import multi.platform.auth.shared.external.AuthConfig
import multi.platform.chat.example.BuildConfig

class AuthConfigImpl(
    override val logo: String = "https://cdn-icons-png.flaticon.com/512/6386/6386852.png",
    override val countryCode: String = "+62",
    override val fbAppId: String = BuildConfig.FB_APP_ID,
    override val googleWebClientId: String = BuildConfig.GOOGLE_WEB_CLIENT_ID,
    override val googleWebClientSecret: String = BuildConfig.GOOGLE_WEB_CLIENT_SECRET,
    override val onesignalAppId: String = BuildConfig.ONESIGNAL_APP_ID,

    override val headerTransactionIdKey: String = "x-header-transaction-id",
    override val signInByEmailApi: String = "/v1/accounts:signInWithPassword?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY",
    override val signInByProviderApi: String = "/v1/accounts:signInWithIdp?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY",
    override val signInByBiometricApi: String = "/coming-soon",
    override val signOutApi: String = "",
    override val registerWithAvatar: Boolean = false,
    override val registerWithBio: Boolean = false,
    override val registerApi: String = "/v1/accounts:signUp?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY",
    override val forgetPasswordApi: String = "/coming-soon",
    override val signInByPhoneApi: String = "/coming-soon",
    override val validatePhoneApi: String = "",
    override val verifyOtpApi: String = "",
) : AuthConfig {

    private fun check(jsonObject: JsonObject) {
        if (jsonObject["success"]?.jsonPrimitive?.boolean == false) {
            throw Exception(jsonObject["message"]?.jsonPrimitive?.contentOrNull)
        }
    }

    override fun signInMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        jsonObject.let {
            Ticket(
                session = Session(
                    id = it["localId"]?.jsonPrimitive?.contentOrNull,
                    displayName = it["displayName"]?.jsonPrimitive?.contentOrNull,
                    email = it["email"]?.jsonPrimitive?.contentOrNull,
                    token = it["idToken"]?.jsonPrimitive?.contentOrNull,
                    refreshToken = it["refreshToken"]?.jsonPrimitive?.contentOrNull,
                ),
            )
        }
    } else {
        null
    }

    override fun signInByProviderMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        jsonObject.let {
            Ticket(
                session = Session(
                    id = it["localId"]?.jsonPrimitive?.contentOrNull,
                    displayName = it["displayName"]?.jsonPrimitive?.contentOrNull,
                    email = it["email"]?.jsonPrimitive?.contentOrNull,
                    token = it["idToken"]?.jsonPrimitive?.contentOrNull,
                    refreshToken = it["refreshToken"]?.jsonPrimitive?.contentOrNull,
                    profilePicUrl = it["photoUrl"]?.jsonPrimitive?.contentOrNull,
                ),
            )
        }
    } else {
        null
    }

    override fun signOutMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        Ticket()
    } else {
        null
    }

    override fun registerMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        jsonObject.let {
            Ticket(
                session = Session(
                    id = it["localId"]?.jsonPrimitive?.contentOrNull,
                    email = it["email"]?.jsonPrimitive?.contentOrNull,
                    token = it["idToken"]?.jsonPrimitive?.contentOrNull,
                    refreshToken = it["refreshToken"]?.jsonPrimitive?.contentOrNull,
                ),
            )
        }
    } else {
        null
    }

    override fun verifyOtpMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        Ticket()
    } else {
        null
    }

    override fun forgetPasswordMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        Ticket()
    } else {
        null
    }

    override fun validatePhoneMapper(jsonObject: JsonObject?): Ticket? = if (jsonObject != null) {
        check(jsonObject)
        Ticket()
    } else {
        null
    }
}
