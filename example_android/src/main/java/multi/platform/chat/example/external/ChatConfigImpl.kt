package multi.platform.chat.example.external

import multi.platform.chat.example.BuildConfig
import multi.platform.chat.shared.external.ChatConfig

class ChatConfigImpl(
    override val logo: String = "https://cdn-icons-png.flaticon.com/512/6386/6386852.png",
    override val chatGrpcServerUrl: String = BuildConfig.CHAT_GRPC_SERVER,
    override val userGrpcServerUrl: String = BuildConfig.USER_GRPC_SERVER,
    override val messageColorSelf: Long = 0xFFE5FEFB,
    override val messageColorOther: Long = 0xFFFFFFFF,
    override val messageColorTime: Long = 0xFF979797,
    override val loadMessageFromFirstTimeJoinRoomOnSameDevice: Boolean = false,
    override val headerTransactionIdKey: String = "x-header-transaction-id",
    override val refreshTokenApi: String = "https://securetoken.googleapis.com/v1/token?key=AIzaSyABjA3Gd-q_gfDzPto21bUuufrjlIJbljY",
) : ChatConfig
