Pod::Spec.new do |spec|
    spec.name                     = 'chat_resources'
    spec.version                  = '1.0.0'
    spec.homepage                 = 'https://gitlab.com/kotlin-multiplatform-mobile/chat'
    spec.source                   = { :http=> ''}
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'Provide Sign In with multiple provider'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '14.1'
    spec.resources = ['chat_shared/build/compose/cocoapods/compose-resources']
    spec.pod_target_xcconfig = {
        'KOTLIN_PROJECT_PATH' => ':chat_shared',
        'PRODUCT_MODULE_NAME' => 'chat_shared',
    }
    spec.script_phases = [
        {
            :name => 'Build chat_shared',
            :execution_position => :before_compile,
            :shell_path => '/bin/sh',
            :script => <<-SCRIPT
                if [ "YES" = "$OVERRIDE_KOTLIN_BUILD_IDE_SUPPORTED" ]; then
                  echo "Skipping Gradle build task invocation due to OVERRIDE_KOTLIN_BUILD_IDE_SUPPORTED environment variable set to \"YES\""
                  exit 0
                fi
                set -ev
                REPO_ROOT="$PODS_TARGET_SRCROOT"
                "$REPO_ROOT/gradlew" -p "$REPO_ROOT" $KOTLIN_PROJECT_PATH:syncFramework \
                    -Pkotlin.native.cocoapods.platform=$PLATFORM_NAME \
                    -Pkotlin.native.cocoapods.archs="$ARCHS" \
                    -Pkotlin.native.cocoapods.configuration="$CONFIGURATION"
            SCRIPT
        }
    ]
end