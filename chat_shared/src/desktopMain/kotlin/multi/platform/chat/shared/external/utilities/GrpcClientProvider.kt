package multi.platform.chat.shared.external.utilities

import com.squareup.wire.GrpcClient
import okhttp3.OkHttpClient
import okhttp3.Protocol.HTTP_1_1
import okhttp3.Protocol.HTTP_2
import java.time.Duration

object GrpcClientProvider {
    operator fun invoke(baseUrl: String, minCompress: Long) = GrpcClient.Builder()
        .minMessageToCompress(minCompress)
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .build()

    private val okHttpClient = OkHttpClient.Builder()
        .readTimeout(Duration.ofMinutes(1))
        .writeTimeout(Duration.ofMinutes(1))
        .callTimeout(Duration.ofMinutes(1))
        .retryOnConnectionFailure(true)
        .protocols(listOf(HTTP_1_1, HTTP_2))
        .build()
}
