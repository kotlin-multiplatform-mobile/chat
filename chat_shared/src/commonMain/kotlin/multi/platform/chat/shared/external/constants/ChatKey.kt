package multi.platform.chat.shared.external.constants

object ChatKey {
    // GRPC KEYS
    const val MESSAGE_TEXT_KEY = "MESSAGE_TEXT"
    const val MESSAGE_TIME_KEY = "MESSAGE_TIME"

    // PREFERENCE KEYS
    const val USER_FIRST_JOIN_ID_KEY = "USER_FIRST_JOIN_ID"
    const val USER_FIRST_JOIN_SECONDS_KEY = "USER_FIRST_JOIN_SECONDS"
    const val USER_FIRST_JOIN_NANO_KEY = "USER_FIRST_JOIN_NANO"

    // NAVIGATION KEYS
    const val CONFLICTED_KEY = "CONFLICTED"
}
