package multi.platform.chat.shared.app.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.core.shared.app.common.compossables.UserAvatarView
import multi.platform.core.shared.app.common.compossables.UserInitialView

@Composable
fun OnlineAvatarView(
    user: User?,
    size: Dp,
    showOnlineIndicator: Boolean = false,
    showBorderStroke: Boolean = false,
    textStyle: TextStyle = MaterialTheme.typography.headlineMedium,
) {
    Box {
        user?.profilePictUrl?.let {
            UserAvatarView(
                modifier = Modifier.align(Alignment.Center),
                size = size,
                profilePictUrl = it,
                borderColor = if (showBorderStroke) MaterialTheme.colorScheme.primary else Color.Transparent,
            )
        } ?: run {
            UserInitialView(
                modifier = Modifier.size(size).align(Alignment.Center),
                textStyle = textStyle.copy(
                    background = MaterialTheme.colorScheme.surfaceVariant,
                ),
                fullName = user?.name.toString(),
            )
        }
        if (user?.isOnline == true && showOnlineIndicator) {
            Box(
                modifier = Modifier
                    .size(15.dp)
                    .padding(2.dp)
                    .background(Color.Green, shape = CircleShape)
                    .align(Alignment.BottomStart),
            )
        }
    }
}
