package multi.platform.chat.shared.app.chatroom.listmessages

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import multi.platform.chat.chat_shared.generated.resources.Res
import multi.platform.chat.chat_shared.generated.resources.chatroom_empty_messages
import multi.platform.chat.chat_shared.generated.resources.chatroom_error_load_messages
import multi.platform.chat.shared.app.common.ScreenInfoView
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.core.shared.app.common.compossables.CoreLoadingView
import multi.platform.core.shared.external.enums.StateEnum
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.koinInject

@Composable
fun ListMessagesView(
    chatConfig: ChatConfig,
    lastSentMessage: Message?,
) {
    val lifecycleOwner = LocalLifecycleOwner.current
    val viewModel = koinInject<ListMessagesViewModel>()
    val listMessagesState = rememberLazyListState()

    lastSentMessage?.takeIf { it != viewModel.viewState.value.lastSentMessage }?.let {
        viewModel.setState {
            copy(
                viewState = StateEnum.SUCCESS,
                lastSentMessage = it,
                messages = (messages + lastSentMessage).distinctBy { it.id.toString() },
            )
        }
    }

    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_START) {
                viewModel.setEvent(ListMessagesContract.Event.LoadMessages)
            } else if (event == Lifecycle.Event.ON_STOP) {
                viewModel.setEvent(ListMessagesContract.Event.DisposeAll)
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    DisposableEffect(Unit) {
        onDispose {
            viewModel.setEvent(ListMessagesContract.Event.DisposeAll)
        }
    }

    if (viewModel.viewState.value.messages.isNotEmpty()) {
        LaunchedEffect(viewModel.viewState.value.messages.last()) {
            listMessagesState.animateScrollToItem(
                viewModel.viewState.value.messages.lastIndex,
                scrollOffset = 2,
            )
        }
    }

    Box(Modifier.fillMaxSize()) {
        when (viewModel.viewState.value.viewState) {
            StateEnum.LOADING -> {
                CoreLoadingView(
                    modifier = Modifier.align(Alignment.Center),
                    isLoading = true,
                    isLinear = false,
                )
            }

            StateEnum.ERROR -> {
                ScreenInfoView(
                    stringResource(Res.string.chatroom_error_load_messages),
                    Icons.Filled.Warning,
                ) {
                    viewModel.setEvent(ListMessagesContract.Event.LoadMessages)
                }
            }

            StateEnum.EMPTY -> {
                ScreenInfoView(stringResource(Res.string.chatroom_empty_messages))
            }

            StateEnum.SUCCESS -> {
                LazyColumn(
                    modifier = Modifier.fillMaxSize().padding(horizontal = 4.dp),
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    state = listMessagesState,
                ) {
                    item {}
                    items(viewModel.viewState.value.messages, key = { it.id.toString() }) {
                        ItemMessageView(chatConfig, it, viewModel.viewState.value.user)
                    }
                    item {}
                }
            }
        }
    }
}
