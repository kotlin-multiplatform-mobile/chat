package multi.platform.chat.shared

import com.google.firestore.v1.FirestoreClient
import com.google.firestore.v1.GrpcFirestoreClient
import multi.platform.chat.shared.external.ChatConfig
import org.koin.dsl.module

class ChatModule(private val chatConfig: ChatConfig) {
    operator fun invoke() = module {
        single<ChatConfig> { chatConfig }
        single<FirestoreClient> { GrpcFirestoreClient(get()) }
        includes(chatModule(chatConfig))
    }
}
