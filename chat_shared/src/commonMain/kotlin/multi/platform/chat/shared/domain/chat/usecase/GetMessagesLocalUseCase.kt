package multi.platform.chat.shared.domain.chat.usecase

import io.realm.kotlin.query.Sort
import multi.platform.chat.shared.domain.chat.ChatRepository
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.core.shared.domain.common.usecase.CoreUseCase

@Suppress("UNCHECKED_CAST")
class GetMessagesLocalUseCase(
    private val chatRepository: ChatRepository,
) : CoreUseCase {
    override suspend fun call(vararg args: Any?): List<Message> =
        chatRepository.getMessagesLocal(
            args[0] as Int,
            args[1] as Int,
            args[2] as String?,
            args[3] as Pair<String, Sort>?,
        )

    override suspend fun onError(e: Exception) {
        throw e
    }
}
