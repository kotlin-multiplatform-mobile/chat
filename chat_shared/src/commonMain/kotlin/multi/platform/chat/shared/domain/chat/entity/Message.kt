package multi.platform.chat.shared.domain.chat.entity

import io.realm.kotlin.types.RealmInstant
import io.realm.kotlin.types.RealmObject
import kotlinx.serialization.Serializable

@Serializable
open class Message : RealmObject {
    var user: User? = null
    var text: String = ""
    var time: RealmInstant? = null
    var id: String? = null
    var hasBeenSent: Boolean = false
    var haveError: Boolean = false
}
