package multi.platform.chat.shared.app.chatroom

import androidx.compose.foundation.Image
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import multi.platform.chat.chat_shared.generated.resources.Res
import multi.platform.chat.chat_shared.generated.resources.chat_bg_dark
import multi.platform.chat.chat_shared.generated.resources.chat_bg_light
import multi.platform.chat.chat_shared.generated.resources.chatroom_close
import multi.platform.chat.chat_shared.generated.resources.chatroom_conflicted
import multi.platform.chat.shared.app.chatroom.listmessages.ListMessagesView
import multi.platform.chat.shared.app.chatroom.listusers.ListUsersView
import multi.platform.chat.shared.app.chatroom.sendmessage.SendMessageView
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.chat.shared.external.constants.ChatKey
import multi.platform.core.shared.app.common.SIDE_EFFECTS_KEY
import multi.platform.core.shared.external.enums.StateEnum
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.koinInject

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChatRoomScreen(
    signOut: State<String>,
    modifier: Modifier = Modifier,
    chatConfig: ChatConfig,
    navController: NavController,
    signOutDestination: String,
    conflicted: State<String>,
) {
    val viewModel = koinInject<ChatRoomViewModel>()
    val lifecycleOwner = LocalLifecycleOwner.current
    val snackbarHostState = remember { SnackbarHostState() }

    signOut.value.takeIf { it.isNotEmpty() }?.let {
        navController.currentBackStackEntry
            ?.savedStateHandle
            ?.set("sign-out", "")
        navController.popBackStack()
    }

    conflicted.value.takeIf { it.isNotEmpty() }?.let {
        BasicAlertDialog(
            onDismissRequest = {},
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false,
            ),
        ) {
            Card(Modifier.fillMaxWidth().clip(RoundedCornerShape(10.dp))) {
                Box(Modifier.padding(22.dp)) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(stringResource(Res.string.chatroom_conflicted), textAlign = TextAlign.Center)
                        Spacer(Modifier.height(12.dp))
                        Button(onClick = {
                            navController.currentBackStackEntry
                                ?.savedStateHandle
                                ?.set(ChatKey.CONFLICTED_KEY, "")
                            navController.popBackStack()
                        }) {
                            Text(stringResource(Res.string.chatroom_close))
                        }
                    }
                }
            }
        }
    }

    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_STOP) {
                viewModel.setEvent(ChatRoomContract.Event.DisposeAll)
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    LaunchedEffect(SIDE_EFFECTS_KEY) {
        viewModel.effect.onEach { effect ->
            when (effect) {
                is ChatRoomContract.Effect.Navigation.ShowSignOutDialog -> {
                    navController.navigate(signOutDestination)
                }

                is ChatRoomContract.Effect.ShowSnackBar -> {
                    snackbarHostState.currentSnackbarData?.dismiss()
                    snackbarHostState.showSnackbar(
                        effect.message,
                        duration = SnackbarDuration.Short,
                    )
                }
            }
        }.collect()
    }

    Scaffold(
        modifier = modifier.imePadding(),
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState) { data ->
                Snackbar(
                    snackbarData = data,
                    containerColor = when (viewModel.viewState.value.viewState) {
                        StateEnum.ERROR -> Color.Red
                        StateEnum.SUCCESS -> Color.Green
                        else -> Color.White
                    },
                    contentColor = when (viewModel.viewState.value.viewState) {
                        StateEnum.ERROR -> Color.White
                        else -> Color.Black
                    },
                )
            }
        },
        topBar = {
            ChatRoomTopBarView(chatConfig, viewModel)
        },
    ) { p ->
        Box(Modifier.fillMaxSize().padding(p)) {
            Image(
                painterResource(if (isSystemInDarkTheme()) Res.drawable.chat_bg_dark else Res.drawable.chat_bg_light),
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop,
            )
            Column(Modifier.fillMaxSize()) {
                ListUsersView(chatConfig, navController)
                Box(Modifier.weight(1f)) {
                    ListMessagesView(chatConfig, viewModel.viewState.value.lastMessageSent)
                }
                SendMessageView({ message -> viewModel.setState { copy(lastMessageSent = message) } })
            }
        }
    }
}
