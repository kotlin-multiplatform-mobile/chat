package multi.platform.chat.shared.external.ktor

import com.squareup.wire.GrpcCall
import com.squareup.wire.GrpcClient
import com.squareup.wire.GrpcMethod
import com.squareup.wire.GrpcStreamingCall
import io.ktor.client.engine.HttpClientEngine
import multi.platform.chat.shared.external.ChatConfig
import okio.Timeout

// interface RPCService<S : Any, R : Any>: RPC {
//    suspend fun call(req: S): R
//    suspend fun CreateDocument(parent: String, collection_id: String, document_id: String, document: Document?): R
//    suspend fun Listen(req: S): R
// }

class KtorGrpcClient(
    private val chatConfig: ChatConfig,
    private val httpClientEngine: HttpClientEngine,
) : GrpcClient() {
    override fun <S : Any, R : Any> newCall(method: GrpcMethod<S, R>): GrpcCall<S, R> {
        return KtorGrpcCall(chatConfig, httpClientEngine, method, mapOf(), mapOf(), Timeout.NONE)
    }

    override fun <S : Any, R : Any> newStreamingCall(method: GrpcMethod<S, R>): GrpcStreamingCall<S, R> {
        return KtorGrpcStreamingCall(chatConfig, httpClientEngine, method, mapOf(), mapOf(), Timeout.NONE)
    }
}
