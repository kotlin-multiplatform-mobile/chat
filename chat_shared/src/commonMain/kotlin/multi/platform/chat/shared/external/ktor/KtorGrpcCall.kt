package multi.platform.chat.shared.external.ktor

import com.squareup.wire.GrpcCall
import com.squareup.wire.GrpcMethod
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.DEFAULT
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.headers
import io.ktor.client.request.request
import io.ktor.client.request.setBody
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.readBytes
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.content.ByteArrayContent
import io.ktor.serialization.kotlinx.json.json
import kotlinx.coroutines.cancel
import multi.platform.chat.shared.external.ChatConfig
import okio.Buffer
import okio.Timeout

class KtorGrpcCall<S : Any, R : Any>(
    private val chatConfig: ChatConfig,
    private val httpClientEngine: HttpClientEngine,
    override val method: GrpcMethod<S, R>,
    override var requestMetadata: Map<String, String>,
    override val responseMetadata: Map<String, String>?,
    override val timeout: Timeout,
) : GrpcCall<S, R> {
    private lateinit var response: HttpResponse

    val client by lazy {
        HttpClient(httpClientEngine) {
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
            install(ContentNegotiation) {
                json()
            }
        }
    }

    override fun cancel() {
        response.call.cancel()
    }

    override fun clone(): GrpcCall<S, R> {
        TODO("Not yet implemented")
    }

    override fun isCanceled(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isExecuted(): Boolean {
        TODO("Not yet implemented")
    }

    override fun executeBlocking(request: S): R {
        TODO("Not yet implemented")
    }

    override suspend fun execute(request: S): R {
//        val grpcMethod = method
//        val parent = if (request is CreateDocumentRequest) request.parent else ""
//        val collectionId = if (request is CreateDocumentRequest) request.collection_id else ""
//        val documentId = if (request is CreateDocumentRequest) request.document_id else ""
//        val document = if (request is CreateDocumentRequest) request.document else null
//        val resp = client.rpc {
//            contentType(ContentType.parse("application/grpc"))
//            url(chatConfig.chatGrpcServerUrl + grpcMethod.path)
//            rpcConfig { serialization { json() } }
//            headers {
//                method = HttpMethod.Post
//                append("te", "trailers")
//                append("grpc-trace-bin", "")
//                append("grpc-accept-encoding", "gzip")
//                requestMetadata.forEach {
//                    append(it.key, it.value)
//                }
//            }
//            val encodedMessage = Buffer()
//            grpcMethod.requestAdapter.encode(encodedMessage, request)
//
//            val buffer = Buffer()
//            buffer.writeByte(0)
//            buffer.writeInt(encodedMessage.size.toInt())
//            buffer.writeAll(encodedMessage)
//            setBody(ByteArrayContent(buffer.readByteArray()))
//        }.withService<RPCService<S,R>>().call(request)
//        return resp
        val resp = client.request(buildRequest(request))
        return method.responseAdapter.decode(resp.readBytes())
    }

    override fun enqueue(request: S, callback: GrpcCall.Callback<S, R>) {
        TODO("Not yet implemented")
    }

    private fun buildRequest(request: S): HttpRequestBuilder {
        val grpcMethod = method
        return HttpRequestBuilder().apply {
            method = HttpMethod.Post
            url(chatConfig.chatGrpcServerUrl + grpcMethod.path)
            headers {
                append(HttpHeaders.ContentType, "application/grpc")
                append("te", "trailers")
                append("grpc-trace-bin", "")
                requestMetadata.forEach {
                    append(it.key, it.value)
                }
            }
            val encodedMessage = Buffer()
            grpcMethod.requestAdapter.encode(encodedMessage, request)

            val buffer = Buffer()
            buffer.writeByte(0)
            buffer.writeInt(encodedMessage.size.toInt())
            buffer.writeAll(encodedMessage)
            setBody(ByteArrayContent(buffer.readByteArray()))
        }
    }
}
