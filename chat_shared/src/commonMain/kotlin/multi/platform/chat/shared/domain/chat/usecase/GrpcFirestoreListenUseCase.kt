@file:Suppress("UNCHECKED_CAST")

package multi.platform.chat.shared.domain.chat.usecase

import com.google.firestore.v1.DocumentChange
import com.google.firestore.v1.DocumentDelete
import com.google.firestore.v1.FirestoreClient
import com.google.firestore.v1.ListenRequest
import com.google.firestore.v1.ListenResponse
import com.google.firestore.v1.StructuredQuery
import com.google.firestore.v1.Target
import com.google.firestore.v1.TargetChange
import com.squareup.wire.GrpcException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import multi.platform.chat.shared.domain.chat.ChatRepository
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.enums.StateEnum
import multi.platform.core.shared.external.utilities.Persistent
import okio.IOException

class GrpcFirestoreListenUseCase(
    private val persistent: Persistent,
    private val firestoreClient: FirestoreClient,
    private val authRepository: ChatRepository,
) : CoreUseCase {

    override suspend fun call(vararg args: Any?): Any? {
        val targetId = args[0] as Int
        val scope = args[1] as CoroutineScope
        val collectionId = args[2] as String
        val filter = args[3] as StructuredQuery.Filter?
        val sort = args[4] as StructuredQuery.Direction
        val order = args[5] as String
        val limit = args[6] as Int

        val onError = args[7] as ((StateEnum, Exception) -> Unit)?
        val onDispose = args[8] as (() -> Unit)?
        val onDocumentChange = args[9] as ((DocumentChange) -> Unit)?
        val onDocumentDelete = args[10] as ((DocumentDelete) -> Unit)?
        val onTargetChange = args[11] as ((TargetChange) -> Unit)?
        val onSuccess =
            args[12] as ((SendChannel<ListenRequest>, ReceiveChannel<ListenResponse>) -> Unit)?

        lateinit var receiveUpdateChannel: ReceiveChannel<ListenResponse>

        firestoreClient.Listen().apply {
            requestMetadata = mapOf(
                "Authorization" to "Bearer ${ChatGlobal.accessToken}",
                "x-goog-request-params" to "database=projects/kmm-example-app/databases/kmm-example-app",
            )
        }.executeIn(scope).let { (sendChannel, receiveChannel) ->
            receiveUpdateChannel = receiveChannel
            scope.launch {
                sendInitialCommand(
                    sendChannel,
                    targetId,
                    collectionId,
                    filter,
                    sort,
                    order,
                    limit,
                )
            }
            onSuccess?.invoke(sendChannel, receiveChannel)
        }

        try {
            for (update in receiveUpdateChannel) {
                println(update.toString())
                update.document_change?.let {
                    onDocumentChange?.invoke(it)
                }
                update.document_delete?.let {
                    onDocumentDelete?.invoke(it)
                }
                update.target_change?.let {
                    onTargetChange?.invoke(it)
                }
            }
        } catch (e: GrpcException) {
            e.printStackTrace()
            if (e.grpcStatus.name == "UNAUTHENTICATED") {
                try {
                    val response = authRepository.refreshToken(ChatGlobal.refreshToken.toString())
                    persistent.putString(CommonKey.ACCESS_TOKEN_KEY, response.first.toString())
                    persistent.putString(CommonKey.REFRESH_TOKEN_KEY, response.second.toString())
                    ChatGlobal.accessToken = response.first
                    ChatGlobal.refreshToken = response.second
                    onDispose?.invoke()
                    this.call(
                        targetId,
                        scope,
                        collectionId,
                        filter,
                        sort,
                        order,
                        limit,
                        onError,
                        onDispose,
                        onDocumentChange,
                        onDocumentDelete,
                        onTargetChange,
                        onSuccess,
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                    onError?.invoke(StateEnum.ERROR, e)
                }
            } else {
                onError?.invoke(StateEnum.ERROR, e)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            delay(1000)
            this.call(
                targetId,
                scope,
                collectionId,
                filter,
                sort,
                order,
                limit,
                onError,
                onDispose,
                onDocumentChange,
                onDocumentDelete,
                onTargetChange,
                onSuccess,
            )
        } catch (e: Exception) {
            e.printStackTrace()
            onError?.invoke(StateEnum.ERROR, e)
        }
        return true
    }

    private fun sendInitialCommand(
        sendCommandChannel: SendChannel<ListenRequest>,
        targetId: Int,
        collectionId: String,
        filter: StructuredQuery.Filter?,
        sort: StructuredQuery.Direction,
        order: String,
        limit: Int,
    ) {
        try {
            val request = ListenRequest(
                database = "projects/kmm-example-app/databases/kmm-example-app",
                add_target = Target(
                    target_id = targetId,
                    query = Target.QueryTarget(
                        parent = "projects/kmm-example-app/databases/kmm-example-app/documents/rooms/${ChatGlobal.roomId}",
                        structured_query = StructuredQuery(
                            from = listOf(
                                StructuredQuery.CollectionSelector(
                                    collection_id = collectionId,
                                ),
                            ),
                            order_by = listOf(
                                StructuredQuery.Order(
                                    field_ = StructuredQuery.FieldReference(
                                        field_path = order,
                                    ),
                                    direction = sort,
                                ),
                            ),
                            where_ = filter,
                            limit = limit,
                        ),
                    ),
                ),
            )
            sendCommandChannel.trySend(request)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
