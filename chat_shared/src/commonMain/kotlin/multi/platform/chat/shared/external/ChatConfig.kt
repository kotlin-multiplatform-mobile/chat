package multi.platform.chat.shared.external

interface ChatConfig {
    val logo: String
    val messageColorSelf: Long
    val messageColorOther: Long
    val messageColorTime: Long
    val loadMessageFromFirstTimeJoinRoomOnSameDevice: Boolean

    val chatGrpcServerUrl: String
    val userGrpcServerUrl: String
    val headerTransactionIdKey: String
    val refreshTokenApi: String

    fun routeToChatRoom() = "/screen/chat/room"
}
