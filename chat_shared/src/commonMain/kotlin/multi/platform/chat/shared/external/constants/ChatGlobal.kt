package multi.platform.chat.shared.external.constants

import com.squareup.wire.Instant
import multi.platform.chat.shared.domain.chat.entity.User

object ChatGlobal {
    var roomId: Int? = null
    var userJoinTime: Instant? = null
    var userFirstJoinId: String? = null
    var userFirstJoinTime: Instant? = null
    var accessToken: String? = null
    var refreshToken: String? = null
    var user: User? = null
}
