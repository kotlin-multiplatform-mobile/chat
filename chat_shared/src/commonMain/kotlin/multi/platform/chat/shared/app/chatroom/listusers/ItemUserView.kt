package multi.platform.chat.shared.app.chatroom.listusers

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import multi.platform.chat.shared.app.common.OnlineAvatarView
import multi.platform.chat.shared.domain.chat.entity.User

@Composable
fun ItemUserView(
    modifier: Modifier = Modifier,
    user: User,
) {
    Column(
        modifier = modifier.width(55.dp).fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        OnlineAvatarView(user, 55.dp, true, true, MaterialTheme.typography.headlineSmall)
        Text(
            modifier = Modifier.padding(top = 4.dp),
            text = if (user.name.length < 4) user.name else user.name.take(4) + "...",
            textAlign = TextAlign.Center,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.bodySmall.copy(color = MaterialTheme.colorScheme.primary),
        )
    }
}
