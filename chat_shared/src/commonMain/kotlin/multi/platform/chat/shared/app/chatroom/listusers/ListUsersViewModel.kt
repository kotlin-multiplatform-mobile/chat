package multi.platform.chat.shared.app.chatroom.listusers

import com.squareup.wire.GrpcException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.chat.shared.domain.chat.usecase.GrpcOnlineUsersUseCase
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.core.shared.app.common.ComposeViewModel
import multi.platform.core.shared.external.enums.StateEnum
import user.User as UserGrpc

@Suppress("KOTLIN:S6305")
class ListUsersViewModel(
    private val grpcOnlineUsersUseCase: GrpcOnlineUsersUseCase,
) : ComposeViewModel<ListUsersContract.Event, ListUsersContract.State, ListUsersContract.Effect>() {

    private var appendJob: Job? = null
    private val addUsers = mutableListOf<User>()
    private val removeUsers = mutableListOf<User>()
    private var sendCommandChannel: SendChannel<UserGrpc>? = null

    private val onlineSoundEffectUrl =
        "https://gitlab.com/kotlin-multiplatform-mobile/chat/-/raw/main/resources/online.mp3"
    private val offlineSoundEffectUrl =
        "https://gitlab.com/kotlin-multiplatform-mobile/chat/-/raw/main/resources/offline.mp3"

    init {
        setState { copy(user = ChatGlobal.user) }
    }

    override fun handleEvents(event: ListUsersContract.Event) {
        when (event) {
            is ListUsersContract.Event.LoadUsers -> {
                listenUsers()
            }

            is ListUsersContract.Event.DisposeAll -> {
                disposeAll()
            }
        }
    }

    override fun setInitialState() = ListUsersContract.State(
        viewState = StateEnum.LOADING,
        users = emptyList(),
        user = null,
        soundEffectUrl = null,
    )

    private fun listenUsers() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.IO + SupervisorJob())
        coroutine.launch {
            setState { copy(viewState = StateEnum.LOADING) }
            val userGrpc = UserGrpc(
                id = viewState.value.user?.id.toString(),
                name = viewState.value.user?.name.toString(),
                avatar = viewState.value.user?.profilePictUrl,
                active = true,
            )
            grpcOnlineUsersUseCase.call(
                userGrpc,
                coroutine,
                { state: StateEnum, e: Exception? ->
                    if (e is GrpcException && e.grpcStatus.name == "ALREADY_EXISTS") {
                        disposeAll()
                        setEffect { ListUsersContract.Effect.Conflict }
                    } else {
                        setState { copy(viewState = state) }
                    }
                },
                { disposeAll() },
                { user: UserGrpc -> onUserChange(user) },
                { sendChannel: SendChannel<UserGrpc>, _: ReceiveChannel<UserGrpc> ->
                    sendCommandChannel = sendChannel
                    ChatGlobal.user?.isOnline = true
                },
            )
        }
    }

    private fun onUserChange(userGrpc: UserGrpc) {
        userGrpc.id.takeIf { it.isNotEmpty() }?.let {
            val user = User().apply {
                id = it
                name = userGrpc.name
                profilePictUrl = userGrpc.avatar
                isOnline = userGrpc.active
            }
            if (viewState.value.user?.id != user.id) {
                if (addUsers.isEmpty() && user.isOnline) {
                    addUsers.add(user)
                } else if (removeUsers.isEmpty() && !user.isOnline) {
                    removeUsers.add(user)
                } else {
                    val idsAdd = addUsers.map { it.id }
                    if (user.id !in idsAdd && user.isOnline) addUsers.add(user)

                    val idsRemove = removeUsers.map { it.id }
                    if (user.id !in idsRemove && !user.isOnline) removeUsers.add(user)
                }
            }
            appendJob?.cancel()
            appendJob = scope.launch {
                delay(300)
                val sizeUsersBefore = viewState.value.users.size

                val existUsers = viewState.value.users.map { it.id }
                val cleanAdd = addUsers.filterNot { it.id in existUsers }
                if (cleanAdd.isNotEmpty()) setState { copy(users = cleanAdd + users) }

                val idsRemove = removeUsers.map { it.id }
                if (idsRemove.isNotEmpty()) {
                    val cleanRemove = viewState.value.users.toMutableList()
                    cleanRemove.removeAll { it.id in idsRemove }
                    setState { copy(users = cleanRemove) }
                }

                val sizeUsersAfter = viewState.value.users.size

                addUsers.clear()
                removeUsers.clear()

                if (sizeUsersAfter > sizeUsersBefore) {
                    setState { copy(soundEffectUrl = onlineSoundEffectUrl) }
                } else if (sizeUsersAfter > sizeUsersBefore) {
                    setState { copy(soundEffectUrl = offlineSoundEffectUrl) }
                }

                if (viewState.value.users.isEmpty() && viewState.value.viewState != StateEnum.EMPTY) {
                    setState { copy(viewState = StateEnum.EMPTY) }
                }
                if (viewState.value.users.isNotEmpty() && viewState.value.viewState != StateEnum.SUCCESS) {
                    setState { copy(viewState = StateEnum.SUCCESS) }
                }
            }
        }
    }

    private fun disposeAll() {
        try {
            appendJob?.cancel()
            sendCommandChannel?.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
