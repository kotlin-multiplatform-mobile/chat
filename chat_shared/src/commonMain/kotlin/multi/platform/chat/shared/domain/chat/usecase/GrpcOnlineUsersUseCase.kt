@file:Suppress("UNCHECKED_CAST")

package multi.platform.chat.shared.domain.chat.usecase

import com.squareup.wire.GrpcException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import multi.platform.core.shared.external.enums.StateEnum
import okio.IOException
import user.BroadcastClient
import user.User

class GrpcOnlineUsersUseCase(
    private val broadcastClient: BroadcastClient,
) : CoreUseCase {

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun call(vararg args: Any?): Any? {
        val userArg = args[0] as User
        val scope = args[1] as CoroutineScope
        val onViewStateChange = args[2] as ((StateEnum, Any?) -> Unit)?
        val onDispose = args[3] as (() -> Unit)?
        val onUserChange = args[4] as ((User) -> Unit)?
        val onSuccess =
            args[5] as ((SendChannel<User>, ReceiveChannel<User>) -> Unit)?

        lateinit var receiveUpdateChannel: ReceiveChannel<User>

        broadcastClient.CreateStream().apply {
            requestMetadata = mapOf(
                "Authorization" to "Bearer ${ChatGlobal.accessToken}",
                "x-goog-request-params" to "database=projects/kmm-example-app/databases/kmm-example-app",
            )
        }.executeIn(scope).let { (sendChannel, receiveChannel) ->
            receiveUpdateChannel = receiveChannel
            scope.launch { sendChannel.send(userArg) }
            onSuccess?.invoke(sendChannel, receiveChannel)
        }

        try {
            for (update in receiveUpdateChannel) {
                onUserChange?.invoke(update)
            }
        } catch (e: GrpcException) {
            e.printStackTrace()
            onViewStateChange?.invoke(StateEnum.ERROR, e)
        } catch (e: IOException) {
            e.printStackTrace()
            delay(1000)
            this.call(userArg, scope, onViewStateChange, onDispose, onUserChange, onSuccess)
        } catch (e: Exception) {
            e.printStackTrace()
            onViewStateChange?.invoke(StateEnum.ERROR, e)
        }

        return true
    }
}
