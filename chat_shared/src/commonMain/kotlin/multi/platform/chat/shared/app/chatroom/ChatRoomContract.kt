package multi.platform.chat.shared.app.chatroom

import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.core.shared.app.common.ViewEvent
import multi.platform.core.shared.app.common.ViewSideEffect
import multi.platform.core.shared.app.common.ViewState
import multi.platform.core.shared.external.enums.StateEnum

class ChatRoomContract {
    sealed class Event : ViewEvent {
        object SignOut : Event()
        object DisposeAll : Event()
    }

    data class State(
        val viewState: StateEnum,
        val lastMessageSent: Message?,
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        data class ShowSnackBar(val message: String) : Effect()
        sealed class Navigation : Effect() {
            object ShowSignOutDialog : Navigation()
        }
    }
}
