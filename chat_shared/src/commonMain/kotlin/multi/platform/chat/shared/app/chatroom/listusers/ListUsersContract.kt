package multi.platform.chat.shared.app.chatroom.listusers

import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.core.shared.app.common.ViewEvent
import multi.platform.core.shared.app.common.ViewSideEffect
import multi.platform.core.shared.app.common.ViewState
import multi.platform.core.shared.external.enums.StateEnum

class ListUsersContract {
    sealed class Event : ViewEvent {
        object LoadUsers : Event()
        object DisposeAll : Event()
    }

    data class State(
        val viewState: StateEnum,
        val users: List<User>,
        val user: User?,
        val soundEffectUrl: String?,
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        object Conflict : Effect()
        sealed class Navigation : Effect() {
            object Back : Effect()
        }
    }
}
