package multi.platform.chat.shared.external.utilities

import androidx.compose.ui.graphics.Color

object ColorProvider {
    val colors = mutableListOf(
        0xFFEA3468,
        0xFFB634EA,
        0xFF349BEA,
    )
    val allColors = colors.toList()
    fun getColor(): Color {
        if (colors.size == 0) {
            colors.addAll(allColors)
        }
        val idx = (0..colors.size - 1).random()
        val color = colors[idx]
        colors.removeAt(idx)
        return Color(color)
    }
}
