package multi.platform.chat.shared.domain.chat.usecase

import com.google.firestore.v1.CreateDocumentRequest
import com.google.firestore.v1.Document
import com.google.firestore.v1.FirestoreClient
import com.google.firestore.v1.Value
import com.squareup.wire.GrpcException
import com.squareup.wire.ofEpochSecond
import io.realm.kotlin.ext.copyFromRealm
import io.realm.kotlin.ext.isManaged
import kotlinx.coroutines.delay
import multi.platform.chat.shared.domain.chat.ChatRepository
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.chat.shared.external.constants.ChatKey
import multi.platform.core.shared.domain.common.usecase.CoreUseCase
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.utilities.Persistent
import okio.IOException

class GrpcSendMessageUseCase(
    private val persistent: Persistent,
    private val firestoreClient: FirestoreClient,
    private val setMessagesLocalUseCase: SetMessagesLocalUseCase,
    private val authRepository: ChatRepository,
) : CoreUseCase {

    var onSuccess: ((Message?) -> Unit)? = null
    var onError: ((Exception?) -> Unit)? = null

    override suspend fun call(vararg args: Any?): Any {
        var message: Message? = null
        try {
            message = args[0] as Message
            val request = CreateDocumentRequest(
                parent = "projects/kmm-example-app/databases/kmm-example-app/documents/rooms/${ChatGlobal.roomId}",
                collection_id = "messages",
                document_id = message.id.toString(),
                document = Document(
                    fields = mapOf(
                        ChatKey.MESSAGE_TEXT_KEY to Value(string_value = message.text),
                        ChatKey.MESSAGE_TIME_KEY to Value(
                            timestamp_value = ofEpochSecond(
                                message.time!!.epochSeconds,
                                message.time!!.nanosecondsOfSecond.toLong(),
                            ),
                        ),
                        CommonKey.LOCAL_ID_KEY to Value(string_value = message.user?.id),
                        CommonKey.DISPLAY_NAME_KEY to Value(string_value = message.user?.name),
                        CommonKey.AVATAR_KEY to Value(string_value = message.user?.profilePictUrl),
                    ),
                ),
            )
            firestoreClient.CreateDocument().apply {
                requestMetadata = mapOf(
                    "Authorization" to "Bearer ${ChatGlobal.accessToken}",
                    "x-goog-request-params" to "project_id=kmm-example-app&database_id=kmm-example-app",
                )
            }.execute(request)
            if (message.isManaged()) message = message.copyFromRealm()
            message.hasBeenSent = true
            setMessagesLocalUseCase.call(listOf(message))
            onSuccess?.invoke(message)
        } catch (e: GrpcException) {
            e.printStackTrace()
            if (e.grpcStatus.name == "UNAUTHENTICATED") {
                try {
                    val response = authRepository.refreshToken(ChatGlobal.refreshToken.toString())
                    persistent.putString(CommonKey.ACCESS_TOKEN_KEY, response.first.toString())
                    persistent.putString(CommonKey.REFRESH_TOKEN_KEY, response.second.toString())
                    ChatGlobal.accessToken = response.first
                    ChatGlobal.refreshToken = response.second
                    this.call(message)
                } catch (e: Exception) {
                    e.printStackTrace()
                    onError?.invoke(e)
                }
            } else if (e.grpcStatus.name == "ALREADY_EXISTS") {
                if (message?.isManaged() == true) message = message.copyFromRealm()
                message?.hasBeenSent = true
                setMessagesLocalUseCase.call(listOf(message))
                onSuccess?.invoke(message)
            } else {
                onError?.invoke(e)
            }
        } catch (e: IOException) {
            e.printStackTrace()
            delay(1000)
            this.call(message)
        } catch (e: Exception) {
            e.printStackTrace()
            onError?.invoke(e)
        }
        return true
    }
}
