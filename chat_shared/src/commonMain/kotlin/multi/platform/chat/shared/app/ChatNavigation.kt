package multi.platform.chat.shared.app

import androidx.compose.runtime.collectAsState
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.navigation
import multi.platform.chat.shared.app.chatroom.ChatRoomScreen
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.chat.shared.external.constants.ChatKey
import multi.platform.core.shared.external.extensions.coreComposable

fun NavGraphBuilder.ChatNavigation(
    chatConfig: ChatConfig,
    navController: NavController,
    routeName: String,
    startDestination: String,
    signOutDestination: String,
) {
    navigation(
        route = routeName,
        startDestination = startDestination,
    ) {
        coreComposable(
            route = chatConfig.routeToChatRoom(),
        ) {
            val signOut = it.savedStateHandle.getStateFlow("sign-out", "").collectAsState()
            val conflicted = it.savedStateHandle.getStateFlow(ChatKey.CONFLICTED_KEY, "").collectAsState()
            ChatRoomScreen(
                signOut = signOut,
                chatConfig = chatConfig,
                navController = navController,
                signOutDestination = signOutDestination,
                conflicted = conflicted,
            )
        }
    }
}
