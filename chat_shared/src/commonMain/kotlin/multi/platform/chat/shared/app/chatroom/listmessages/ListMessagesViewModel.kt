package multi.platform.chat.shared.app.chatroom.listmessages

import com.google.firestore.v1.DocumentChange
import com.google.firestore.v1.DocumentDelete
import com.google.firestore.v1.ListenRequest
import com.google.firestore.v1.ListenResponse
import com.google.firestore.v1.StructuredQuery
import com.google.firestore.v1.TargetChange
import com.google.firestore.v1.Value
import io.realm.kotlin.ext.copyFromRealm
import io.realm.kotlin.ext.isManaged
import io.realm.kotlin.query.Sort
import io.realm.kotlin.types.RealmInstant
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.IO
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.chat.shared.domain.chat.usecase.GetMessagesLocalUseCase
import multi.platform.chat.shared.domain.chat.usecase.GrpcFirestoreListenUseCase
import multi.platform.chat.shared.domain.chat.usecase.GrpcSendMessageUseCase
import multi.platform.chat.shared.domain.chat.usecase.SetMessagesLocalUseCase
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.chat.shared.external.constants.ChatKey.MESSAGE_TEXT_KEY
import multi.platform.chat.shared.external.constants.ChatKey.MESSAGE_TIME_KEY
import multi.platform.core.shared.app.common.ComposeViewModel
import multi.platform.core.shared.external.constants.CommonKey.AVATAR_KEY
import multi.platform.core.shared.external.constants.CommonKey.DISPLAY_NAME_KEY
import multi.platform.core.shared.external.constants.CommonKey.LOCAL_ID_KEY
import multi.platform.core.shared.external.enums.StateEnum
import kotlin.random.Random

@Suppress("KOTLIN:S6305")
class ListMessagesViewModel(
    private val chatConfig: ChatConfig,
    private val getMessagesLocalUseCase: GetMessagesLocalUseCase,
    private val setMessagesLocalUseCase: SetMessagesLocalUseCase,
    private val grpcSendMessageUseCase: GrpcSendMessageUseCase,
    private val grpcFirestoreListenUseCase: GrpcFirestoreListenUseCase,
) : ComposeViewModel<ListMessagesContract.Event, ListMessagesContract.State, ListMessagesContract.Effect>() {

    private var limit = 10
    private var page = 0
    private var appendJob: Job? = null
    private val tempMessages = mutableListOf<Message>()
    private var targetId = Random.nextInt(1000000000)
    private var sendCommandChannel: SendChannel<ListenRequest>? = null

    init {
        setState { copy(user = ChatGlobal.user) }
    }

    override fun handleEvents(event: ListMessagesContract.Event) {
        when (event) {
            is ListMessagesContract.Event.LoadMessages -> {
                if (viewState.value.messages.isEmpty()) getMessageFromLocal()
                listenMessages()
            }

            is ListMessagesContract.Event.DisposeAll -> disposeAll()
        }
    }

    override fun setInitialState() = ListMessagesContract.State(
        viewState = StateEnum.LOADING,
        messages = emptyList(),
        lastSentMessage = null,
        user = null,
    )

    private fun getMessageFromLocal() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            val results = getMessagesLocalUseCase.call(
                limit * page,
                limit,
                null,
                Pair("time", Sort.DESCENDING),
            ) as MutableList<Message>
            if (results.isNotEmpty()) {
                setState {
                    copy(
                        messages = results.reversed() + messages,
                        viewState = StateEnum.SUCCESS,
                    )
                }
                results.filter { !it.hasBeenSent }.forEach {
                    // MARK: Resend offline message
                    grpcSendMessageUseCase.apply {
                        onSuccess = {
                            checkNeedUpdate(listOf(it?.id))
                        }
                    }.call(it.copyFromRealm())
                }
            } else {
                setState { copy(viewState = StateEnum.EMPTY) }
            }
        }
    }

    private fun listenMessages() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.IO + SupervisorJob())
        coroutine.launch {
            val filter = StructuredQuery.Filter(
                field_filter = StructuredQuery.FieldFilter(
                    field_ = StructuredQuery.FieldReference(field_path = MESSAGE_TIME_KEY),
                    op = StructuredQuery.FieldFilter.Operator.GREATER_THAN_OR_EQUAL,
                    // MARK: Only listen new messages or from first time join room
                    value_ = Value(timestamp_value = if (chatConfig.loadMessageFromFirstTimeJoinRoomOnSameDevice) ChatGlobal.userFirstJoinTime else ChatGlobal.userJoinTime),
                ),
            )
            val sort = StructuredQuery.Direction.ASCENDING
            grpcFirestoreListenUseCase.call(
                targetId, coroutine, "messages", filter, sort, MESSAGE_TIME_KEY, limit,
                { state: StateEnum, _: Exception ->
                    setState { copy(viewState = state) }
                },
                { disposeAll() },
                { doc: DocumentChange -> onDocumentChange(doc) },
                { doc: DocumentDelete -> onDocumentDelete(doc) },
                { tar: TargetChange -> onTargetChange(tar) },
                { sendChannel: SendChannel<ListenRequest>, _: ReceiveChannel<ListenResponse> -> sendCommandChannel = sendChannel },
            )
        }
    }

    private fun onDocumentChange(doc: DocumentChange) {
        doc.document?.let { d ->
            val message = documentToMessage(d)
            if (tempMessages.isEmpty()) {
                tempMessages.add(message)
            } else {
                val ids = tempMessages.map { it.id }
                if (message.id !in ids) tempMessages.add(message)
            }
            appendJob?.cancel()
            appendJob = scope.launch {
                delay(300)
                val ids = tempMessages.map { it.id }
                checkNeedUpdate(ids)
                setMessagesLocalUseCase.call(tempMessages)
                val clean = tempMessages.filter { it !in viewState.value.messages }
                    .filter { it.text != viewState.value.lastSentMessage?.text }
                if (clean.isNotEmpty()) setState { copy(messages = (messages + clean).distinctBy { it.id.toString() }) }
                tempMessages.clear()
                setState { copy(viewState = if (messages.isEmpty()) StateEnum.EMPTY else StateEnum.SUCCESS) }
            }
        }
    }

    private fun checkNeedUpdate(ids: List<String?>) {
        val needUpdate = viewState.value.messages.toMutableList()
        viewState.value.messages.filter { !it.hasBeenSent && it.id in ids }
            .takeIf { it.isNotEmpty() }?.let { msgs ->
                msgs.forEach { message ->
                    var msg = message
                    if (msg.isManaged()) msg = msg.copyFromRealm()
                    msg.hasBeenSent = true
                    val index = needUpdate.indexOf(message)
                    needUpdate[index] = msg
                }
                setState { copy(messages = needUpdate) }
            }
    }

    private fun onDocumentDelete(doc: DocumentDelete) {
        setState { copy(messages = messages.filter { it.id != doc.document.split("/").last() }) }
    }

    private fun onTargetChange(target: TargetChange) {
        if (target.target_change_type == TargetChange.TargetChangeType.ADD) {
            appendJob?.cancel()
            appendJob = scope.launch {
                delay(300)
                setState { copy(viewState = if (messages.isEmpty()) StateEnum.EMPTY else StateEnum.SUCCESS) }
            }
        }
    }

    private fun documentToMessage(document: com.google.firestore.v1.Document): Message {
        return Message().apply {
            user = User().apply {
                id = document.fields[LOCAL_ID_KEY]?.string_value.toString()
                name = document.fields[DISPLAY_NAME_KEY]?.string_value.toString()
                profilePictUrl = document.fields[AVATAR_KEY]?.string_value
            }
            text = document.fields[MESSAGE_TEXT_KEY]?.string_value.toString()
            document.fields[MESSAGE_TIME_KEY]?.timestamp_value?.let {
                time = RealmInstant.from(it.getEpochSecond(), it.getNano())
            }
            id = document.name.split("/").last()
            hasBeenSent = true
        }
    }

    private fun disposeAll() {
        appendJob?.cancel()
        try {
            val request = ListenRequest(
                database = "projects/kmm-example-app/databases/kmm-example-app",
                remove_target = targetId,
            )
            sendCommandChannel?.trySend(request)
            sendCommandChannel?.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
