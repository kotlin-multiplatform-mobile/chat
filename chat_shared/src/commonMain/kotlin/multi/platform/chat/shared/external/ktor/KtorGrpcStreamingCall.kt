package multi.platform.chat.shared.external.ktor

import com.squareup.wire.GrpcMethod
import com.squareup.wire.GrpcStreamingCall
import com.squareup.wire.MessageSink
import com.squareup.wire.MessageSource
import io.ktor.client.HttpClient
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.DEFAULT
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.serialization.kotlinx.json.json
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.launch
import multi.platform.chat.shared.external.ChatConfig
import okio.Timeout

class KtorGrpcStreamingCall<S : Any, R : Any>(
    private val chatConfig: ChatConfig,
    private val httpClientEngine: HttpClientEngine,
    override val method: GrpcMethod<S, R>,
    override var requestMetadata: Map<String, String>,
    override val responseMetadata: Map<String, String>?,
    override val timeout: Timeout,
) : GrpcStreamingCall<S, R> {

    val client by lazy {
        HttpClient(httpClientEngine) {
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
            install(ContentNegotiation) {
                json()
            }
        }
    }

    override fun cancel() {
        TODO("Not yet implemented")
    }

    override fun clone(): GrpcStreamingCall<S, R> {
        TODO("Not yet implemented")
    }

    override fun execute(): Pair<SendChannel<S>, ReceiveChannel<R>> {
        TODO("Not yet implemented")
    }

    override fun executeBlocking(): Pair<MessageSink<S>, MessageSource<R>> {
        TODO("Not yet implemented")
    }

    override fun executeIn(scope: CoroutineScope): Pair<SendChannel<S>, ReceiveChannel<R>> {
        val requestChannel = Channel<S>(1)
        val responseChannel = Channel<R>(1)
        val grpcMethod = method
        scope.launch {
//            client.rpc {
//                method = HttpMethod.Post
//                contentType(ContentType.parse("application/grpc"))
//                url(chatConfig.chatServerUrl + grpcMethod.path)
//                rpcConfig { serialization { json() } }
//                headers {
//                    append("te", "trailers")
//                    append("grpc-trace-bin", "")
//                    append("grpc-accept-encoding", "gzip")
//                    requestMetadata.forEach {
//                        append(it.key, it.value)
//                    }
//                }
//            }.withService<RPCService<S,R>>().Listen(requestChannel.receive())
        }
        return requestChannel to responseChannel
    }

    override fun isCanceled(): Boolean {
        TODO("Not yet implemented")
    }

    override fun isExecuted(): Boolean {
        TODO("Not yet implemented")
    }
}
