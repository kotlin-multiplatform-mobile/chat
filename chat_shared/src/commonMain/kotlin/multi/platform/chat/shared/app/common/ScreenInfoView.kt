package multi.platform.chat.shared.app.common

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import multi.platform.chat.chat_shared.generated.resources.Res
import multi.platform.chat.chat_shared.generated.resources.chatroom_retry
import org.jetbrains.compose.resources.stringResource

@Composable
fun ScreenInfoView(
    message: String,
    icon: ImageVector? = null,
    onRetry: (() -> Unit)? = null,
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
    ) {
        icon?.let {
            Icon(
                it,
                contentDescription = null,
                modifier = Modifier.size(100.dp),
            )
            Spacer(Modifier.height(16.dp))
        }
        Text(
            text = message,
            style = MaterialTheme.typography.bodyLarge,
        )
        onRetry?.let {
            Spacer(Modifier.height(8.dp))
            Button(onClick = { it.invoke() }) {
                Text(text = stringResource(Res.string.chatroom_retry))
            }
        }
    }
}
