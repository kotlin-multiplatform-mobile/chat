@file:Suppress("UNCHECKED_CAST")

package multi.platform.chat.shared.domain.chat.usecase

import multi.platform.chat.shared.domain.chat.ChatRepository
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.core.shared.domain.common.usecase.CoreUseCase

class SetMessagesLocalUseCase(
    private val chatRepository: ChatRepository,
) : CoreUseCase {
    override suspend fun call(vararg args: Any?): Boolean =
        chatRepository.setMessagesLocal(args[0] as List<Message>)

    override suspend fun onError(e: Exception) {
        throw e
    }
}
