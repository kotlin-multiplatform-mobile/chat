package multi.platform.chat.shared.app.chatroom.sendmessage

import io.realm.kotlin.types.RealmInstant
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.domain.chat.usecase.GrpcSendMessageUseCase
import multi.platform.chat.shared.domain.chat.usecase.SetMessagesLocalUseCase
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.core.shared.app.common.ComposeViewModel

@Suppress("KOTLIN:S6305")
class SendMessageViewModel(
    private val grpcSendMessageUseCase: GrpcSendMessageUseCase,
    private val setMessagesLocalUseCase: SetMessagesLocalUseCase,
) : ComposeViewModel<SendMessageContract.Event, SendMessageContract.State, SendMessageContract.Effect>() {

    override fun handleEvents(event: SendMessageContract.Event) {
        when (event) {
            is SendMessageContract.Event.SendMessage -> sendMessage()
        }
    }

    override fun setInitialState() = SendMessageContract.State(
        isLoading = false,
        isError = false,
        message = "",
    )

    private fun sendMessage() {
        var coroutine = scope
        if (useAsyncNetworkCall) coroutine = CoroutineScope(Dispatchers.Default)
        coroutine.launch {
            val now = RealmInstant.now()
            val message = Message().apply {
                id = ChatGlobal.user!!.id + "_" + now.epochSeconds
                user = ChatGlobal.user!!
                text = viewState.value.message
                time = now
            }
            setState { copy(message = "") }
            setEffect { SendMessageContract.Effect.ShowMessage(message) }
            setMessagesLocalUseCase.call(listOf(message))
            grpcSendMessageUseCase.apply {
                onSuccess = {
                    setEffect {
                        SendMessageContract.Effect.ShowMessage(
                            Message().apply {
                                id = it?.id
                                user = it?.user
                                text = it?.text ?: ""
                                time = it?.time
                                hasBeenSent = true
                            },
                        )
                    }
                }
                onError = { exception ->
                    exception?.let {
                        setState { copy(isLoading = isLoading, isError = isError) }
                        setEffect { SendMessageContract.Effect.MessageError(it) }
                    }
                }
            }.call(message)
        }
    }
}
