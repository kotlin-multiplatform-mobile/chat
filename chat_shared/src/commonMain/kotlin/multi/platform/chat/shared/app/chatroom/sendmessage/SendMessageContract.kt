package multi.platform.chat.shared.app.chatroom.sendmessage

import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.core.shared.app.common.ViewEvent
import multi.platform.core.shared.app.common.ViewSideEffect
import multi.platform.core.shared.app.common.ViewState

class SendMessageContract {
    sealed class Event : ViewEvent {
        object SendMessage : Event()
    }

    data class State(
        val isLoading: Boolean,
        val isError: Boolean,
        val message: String,
    ) : ViewState

    sealed class Effect : ViewSideEffect {
        data class ShowMessage(val message: Message) : Effect()
        data class MessageError(val error: Exception) : Effect()
    }
}
