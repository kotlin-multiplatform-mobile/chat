package multi.platform.chat.shared.data.chat

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.request.url
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.query.Sort
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonPrimitive
import multi.platform.chat.shared.data.chat.network.payload.RefreshTokenPayload
import multi.platform.chat.shared.domain.chat.ChatRepository
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.core.shared.external.utilities.network.ApiClientProvider

class ChatRepositoryImpl(
    private val realm: Realm,
    private val chatConfig: ChatConfig,
    private val apiClientProvider: ApiClientProvider<HttpClient>,
) : ChatRepository {
    override suspend fun refreshToken(token: String): Pair<String?, String?> {
        val response: JsonObject? = apiClientProvider.client.post {
            url(chatConfig.refreshTokenApi)
            contentType(ContentType.Application.Json)
            setBody(
                RefreshTokenPayload(
                    grant_type = "refresh_token",
                    refresh_token = token,
                ),
            )
        }.body()
        return Pair(
            response?.get("id_token")?.jsonPrimitive?.content,
            response?.get("refresh_token")?.jsonPrimitive?.content,
        )
    }

    override suspend fun setMessagesLocal(messages: List<Message>): Boolean {
        realm.write {
            for (message in messages) {
                val exist = query<Message>("id = $0", message.id).find().firstOrNull()
                val isNew = exist == null

                var temp = Message()
                if (!isNew && exist != null) temp = exist
                temp.id = message.id
                temp.text = message.text
                temp.time = message.time
                temp.hasBeenSent = message.hasBeenSent
                temp.haveError = message.haveError
                if (temp.user != message.user) temp.user = message.user
                if (isNew) copyToRealm(temp)
            }
            true
        }
        return true
    }

    override suspend fun getMessagesLocal(
        offset: Int,
        limit: Int,
        search: String?,
        order: Pair<String, Sort>?,
    ): List<Message> {
        var result = realm.query<Message>(search ?: "id != '0'")
        order?.let { result = result.sort(it) }
        return result.limit(limit).find()
            .toMutableList()
    }
}
