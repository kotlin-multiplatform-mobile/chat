package multi.platform.chat.shared.data.chat.network.payload

import kotlinx.serialization.Serializable

@Serializable
data class RefreshTokenPayload(
    val grant_type: String,
    val refresh_token: String?,
)
