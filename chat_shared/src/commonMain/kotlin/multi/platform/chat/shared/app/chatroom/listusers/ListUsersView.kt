package multi.platform.chat.shared.app.chatroom.listusers

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.NavController
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import multi.platform.chat.chat_shared.generated.resources.Res
import multi.platform.chat.chat_shared.generated.resources.chatroom_empty_users
import multi.platform.chat.chat_shared.generated.resources.chatroom_error_load_users
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.chat.shared.external.constants.ChatKey.CONFLICTED_KEY
import multi.platform.core.shared.app.common.SIDE_EFFECTS_KEY
import multi.platform.core.shared.external.enums.StateEnum
import multi.platform.core.shared.external.utilities.media.AudioPlayer
import multi.platform.core.shared.external.utilities.media.rememberPlayerState
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.koinInject

@Composable
fun ListUsersView(
    chatConfig: ChatConfig,
    navController: NavController,
) {
    val lifecycleOwner = LocalLifecycleOwner.current
    val viewModel = koinInject<ListUsersViewModel>()
    val listState = rememberLazyListState()
    val playerState = rememberPlayerState()
    val player = AudioPlayer(playerState)

    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_START) {
                viewModel.setEvent(ListUsersContract.Event.LoadUsers)
            } else if (event == Lifecycle.Event.ON_STOP) {
                viewModel.setEvent(ListUsersContract.Event.DisposeAll)
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    DisposableEffect(Unit) {
        onDispose {
            viewModel.setEvent(ListUsersContract.Event.DisposeAll)
        }
    }

    if (viewModel.viewState.value.users.isNotEmpty()) {
        LaunchedEffect(viewModel.viewState.value.users.last()) {
            listState.animateScrollToItem(0, scrollOffset = 1)
        }
    }

    viewModel.viewState.value.soundEffectUrl?.let {
        player.replaceSongsUrls(listOf(it))
        player.play()
        viewModel.setState { copy(soundEffectUrl = null) }
    } ?: run { player.replaceSongsUrls(listOf()) }

    LaunchedEffect(SIDE_EFFECTS_KEY) {
        viewModel.effect.onEach { effect ->
            when (effect) {
                is ListUsersContract.Effect.Navigation.Back -> {
                    if (navController.currentBackStackEntry?.destination?.route == chatConfig.routeToChatRoom()) {
                        navController.popBackStack()
                    }
                }
                is ListUsersContract.Effect.Conflict -> {
                    navController.currentBackStackEntry
                        ?.savedStateHandle?.set(CONFLICTED_KEY, "1")
                }
            }
        }.collect()
    }

    Row(
        modifier = Modifier
            .background(MaterialTheme.colorScheme.background)
            .fillMaxWidth()
            .height(90.dp)
            .drawWithContent {
                drawContent()
                drawLine(
                    color = Color.Gray,
                    start = Offset(0f, size.height),
                    end = Offset(size.width, size.height),
                    strokeWidth = 1f,
                )
            },
        verticalAlignment = Alignment.CenterVertically,
    ) {
        viewModel.viewState.value.user?.let {
            ItemUserView(Modifier.padding(horizontal = 16.dp), it)
        }

        VerticalDivider(
            color = Color.Gray,
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .width(1.dp).height(50.dp),
        )

        when (viewModel.viewState.value.viewState) {
            StateEnum.LOADING -> {
                LinearProgressIndicator(
                    Modifier.fillMaxWidth().padding(horizontal = 16.dp).height(1.dp),
                )
            }

            StateEnum.ERROR -> {
                Text(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    text = stringResource(Res.string.chatroom_error_load_users),
                )
            }

            StateEnum.EMPTY -> {
                Text(
                    modifier = Modifier.padding(horizontal = 16.dp),
                    text = stringResource(Res.string.chatroom_empty_users),
                )
            }

            StateEnum.SUCCESS -> {
                LazyRow(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(16.dp),
                    state = listState,
                ) {
                    item {}
                    items(viewModel.viewState.value.users, key = { it.id }) {
                        ItemUserView(user = it)
                    }
                    item {}
                }
            }
        }
    }
}
