package multi.platform.chat.shared.app.chatroom.listmessages

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DoneAll
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import multi.platform.chat.shared.app.common.OnlineAvatarView
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.chat.shared.external.utilities.ColorProvider
import multi.platform.core.shared.external.utilities.timeToString

@Composable
fun Triangle(risingToTheRight: Boolean, background: Color) {
    Box(
        Modifier
            .clip(TriangleEdgeShape(risingToTheRight))
            .background(background)
            .size(6.dp),
    )
}

@Composable
inline fun ItemMessageView(
    chatConfig: ChatConfig,
    message: Message,
    user: User?,
) {
    val isMyMessage = message.user?.id == user?.id

    Box(
        modifier = Modifier.fillMaxWidth(),
        contentAlignment = if (isMyMessage) Alignment.CenterEnd else Alignment.CenterStart,
    ) {
        Row(verticalAlignment = Alignment.Bottom) {
            if (!isMyMessage) {
                OnlineAvatarView(
                    message.user,
                    40.dp,
                    false,
                    false,
                    MaterialTheme.typography.bodyLarge,
                )
                Spacer(Modifier.size(2.dp))
                Triangle(true, Color(chatConfig.messageColorOther))
            } else {
                Spacer(Modifier.width(70.dp))
            }

            Box(
                Modifier.clip(
                    RoundedCornerShape(
                        10.dp,
                        10.dp,
                        if (!isMyMessage) 10.dp else 0.dp,
                        if (!isMyMessage) 0.dp else 10.dp,
                    ),
                )
                    .background(
                        color = if (!isMyMessage) {
                            Color(chatConfig.messageColorOther)
                        } else {
                            Color(
                                chatConfig.messageColorSelf,
                            )
                        },
                    )
                    .alpha(if (message.hasBeenSent) 1f else 0.7f)
                    .padding(horizontal = 10.dp, vertical = 5.dp)
                    .weight(1f, false),
            ) {
                Column {
                    if (!isMyMessage) {
                        Text(
                            text = message.user?.name ?: "",
                            style = MaterialTheme.typography.bodySmall,
                            color = message.user?.color ?: ColorProvider.getColor(),
                        )
                    }
                    Spacer(Modifier.height(2.dp))
                    Text(
                        text = message.text,
                        style = MaterialTheme.typography.bodyMedium,
                        color = Color.Black,
                    )
                    Spacer(Modifier.height(2.dp))
                    Row(
                        Modifier.align(Alignment.End),
                        verticalAlignment = Alignment.CenterVertically,
                    ) {
                        Text(
                            text = timeToString(message.time!!.epochSeconds),
                            textAlign = TextAlign.End,
                            style = MaterialTheme.typography.labelSmall,
                            color = Color(chatConfig.messageColorTime),
                        )
                        if (isMyMessage) {
                            Spacer(Modifier.width(8.dp))
                            if (!message.hasBeenSent) {
                                CircularProgressIndicator(
                                    Modifier.size(15.dp),
                                    strokeWidth = 2.dp,
                                    color = Color(chatConfig.messageColorTime),
                                )
                            } else {
                                Icon(
                                    Icons.Filled.DoneAll,
                                    contentDescription = null,
                                    modifier = Modifier.size(15.dp),
                                    tint = Color(chatConfig.messageColorTime),
                                )
                            }
                        }
                    }
                }
            }

            if (isMyMessage) {
                Triangle(false, Color(chatConfig.messageColorSelf))
            } else {
                Spacer(Modifier.width(70.dp))
            }
        }
    }
}

class TriangleEdgeShape(val risingToTheRight: Boolean) : Shape {
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density,
    ): Outline {
        val trianglePath = if (risingToTheRight) {
            Path().apply {
                moveTo(x = 0f, y = size.height)
                lineTo(x = size.width, y = 0f)
                lineTo(x = size.width, y = size.height)
            }
        } else {
            Path().apply {
                moveTo(x = 0f, y = 0f)
                lineTo(x = size.width, y = size.height)
                lineTo(x = 0f, y = size.height)
            }
        }

        return Outline.Generic(path = trianglePath)
    }
}
