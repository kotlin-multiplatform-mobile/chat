package multi.platform.chat.shared.domain.chat.entity

import androidx.compose.ui.graphics.Color
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Ignore
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import multi.platform.chat.shared.external.utilities.ColorProvider

@Serializable
open class User : RealmObject {
    var id: String = ""
    var name: String = ""
    var profilePictUrl: String? = null
    var isOnline: Boolean = false

    @Ignore
    @Contextual
    var color: Color = ColorProvider.getColor()
}
