package multi.platform.chat.shared.domain.chat

import io.realm.kotlin.query.Sort
import multi.platform.chat.shared.domain.chat.entity.Message

interface ChatRepository {
    suspend fun refreshToken(token: String): Pair<String?, String?>
    suspend fun setMessagesLocal(messages: List<Message>): Boolean
    suspend fun getMessagesLocal(
        offset: Int,
        limit: Int,
        search: String?,
        order: Pair<String, Sort>?,
    ): List<Message>
}
