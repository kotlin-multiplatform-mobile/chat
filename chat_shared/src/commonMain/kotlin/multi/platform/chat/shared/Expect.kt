package multi.platform.chat.shared

import multi.platform.chat.shared.external.ChatConfig
import org.koin.core.module.Module

expect fun chatModule(chatConfig: ChatConfig): Module
internal const val chatDataStoreFileName = "ch4t.preferences_pb"
