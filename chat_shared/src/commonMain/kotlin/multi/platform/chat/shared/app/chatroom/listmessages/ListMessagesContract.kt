package multi.platform.chat.shared.app.chatroom.listmessages

import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.core.shared.app.common.ViewEvent
import multi.platform.core.shared.app.common.ViewSideEffect
import multi.platform.core.shared.app.common.ViewState
import multi.platform.core.shared.external.enums.StateEnum

class ListMessagesContract {
    sealed class Event : ViewEvent {
        object LoadMessages : Event()
        object DisposeAll : Event()
    }

    data class State(
        val viewState: StateEnum,
        val lastSentMessage: Message?,
        val messages: List<Message>,
        val user: User?,
    ) : ViewState

    sealed class Effect : ViewSideEffect
}
