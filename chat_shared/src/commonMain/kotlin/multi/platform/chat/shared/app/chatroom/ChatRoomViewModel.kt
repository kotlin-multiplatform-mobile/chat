package multi.platform.chat.shared.app.chatroom

import com.squareup.wire.ofEpochSecond
import io.realm.kotlin.Realm
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import multi.platform.chat.shared.domain.chat.entity.User
import multi.platform.chat.shared.external.constants.ChatGlobal
import multi.platform.chat.shared.external.constants.ChatKey.USER_FIRST_JOIN_ID_KEY
import multi.platform.chat.shared.external.constants.ChatKey.USER_FIRST_JOIN_NANO_KEY
import multi.platform.chat.shared.external.constants.ChatKey.USER_FIRST_JOIN_SECONDS_KEY
import multi.platform.core.shared.app.common.ComposeViewModel
import multi.platform.core.shared.external.constants.CommonKey
import multi.platform.core.shared.external.constants.CommonKey.ACCESS_TOKEN_KEY
import multi.platform.core.shared.external.constants.CommonKey.AVATAR_KEY
import multi.platform.core.shared.external.constants.CommonKey.DISPLAY_NAME_KEY
import multi.platform.core.shared.external.constants.CommonKey.LOCAL_ID_KEY
import multi.platform.core.shared.external.enums.StateEnum
import multi.platform.core.shared.external.utilities.Persistent

@Suppress("KOTLIN:S6305")
class ChatRoomViewModel(
    private val realm: Realm,
    private val persistent: Persistent,
) : ComposeViewModel<ChatRoomContract.Event, ChatRoomContract.State, ChatRoomContract.Effect>() {

    init {
        ChatGlobal.apply {
            roomId = 1
            accessToken = persistent.getString(ACCESS_TOKEN_KEY, null)
            refreshToken = persistent.getString(CommonKey.REFRESH_TOKEN_KEY, null)
            user = User().apply {
                id = persistent.getString(LOCAL_ID_KEY, "") ?: ""
                name = persistent.getString(DISPLAY_NAME_KEY, "") ?: ""
                profilePictUrl = persistent.getString(AVATAR_KEY, null)
            }
        }
        val userFirstJoinSeconds = persistent.getString(USER_FIRST_JOIN_SECONDS_KEY, null)
        val userFirstJoinNano = persistent.getString(USER_FIRST_JOIN_NANO_KEY, null)
        if (userFirstJoinSeconds != null && userFirstJoinNano != null) {
            ChatGlobal.userFirstJoinTime = ofEpochSecond(userFirstJoinSeconds.toString().toLong(), userFirstJoinNano.toString().toLong())
            val userFirstJoinId = persistent.getString(USER_FIRST_JOIN_ID_KEY, null)
            if (userFirstJoinId != ChatGlobal.user?.id) setupFirstJoin()
        } else {
            setupFirstJoin()
        }
        val now = Clock.System.now()
        ChatGlobal.userJoinTime = ofEpochSecond(now.epochSeconds, now.nanosecondsOfSecond.toLong())
    }

    override fun handleEvents(event: ChatRoomContract.Event) {
        when (event) {
            is ChatRoomContract.Event.SignOut ->
                setEffect { ChatRoomContract.Effect.Navigation.ShowSignOutDialog }
            is ChatRoomContract.Event.DisposeAll -> disposeAll()
        }
    }

    override fun setInitialState() = ChatRoomContract.State(
        viewState = StateEnum.LOADING,
        lastMessageSent = null,
    )

    private fun setupFirstJoin() {
        scope.launch { realm.write { deleteAll() } }
        ChatGlobal.userFirstJoinId = ChatGlobal.user?.id.toString()
        persistent.putString(USER_FIRST_JOIN_ID_KEY, ChatGlobal.userFirstJoinId.toString())
        val now = Clock.System.now()
        ChatGlobal.userJoinTime = ofEpochSecond(now.epochSeconds, now.nanosecondsOfSecond.toLong())
        persistent.putString(USER_FIRST_JOIN_SECONDS_KEY, now.epochSeconds.toString())
        persistent.putString(USER_FIRST_JOIN_NANO_KEY, now.nanosecondsOfSecond.toString())
    }

    private fun disposeAll() {}
}
