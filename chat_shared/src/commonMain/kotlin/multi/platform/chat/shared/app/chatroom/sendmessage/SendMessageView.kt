package multi.platform.chat.shared.app.chatroom.sendmessage

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Send
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import multi.platform.chat.chat_shared.generated.resources.Res
import multi.platform.chat.chat_shared.generated.resources.chatroom_input_message
import multi.platform.chat.shared.domain.chat.entity.Message
import multi.platform.core.shared.app.common.SIDE_EFFECTS_KEY
import org.jetbrains.compose.resources.stringResource
import org.koin.compose.koinInject

@Composable
fun SendMessageView(
    onSuccess: ((Message) -> Unit)? = null,
    onError: ((Exception) -> Unit)? = null,
) {
    val viewModel = koinInject<SendMessageViewModel>()
    val keyboardController = LocalSoftwareKeyboardController.current

    LaunchedEffect(SIDE_EFFECTS_KEY) {
        viewModel.effect.onEach { effect ->
            when (effect) {
                is SendMessageContract.Effect.ShowMessage -> {
                    onSuccess?.invoke(effect.message)
                }
                is SendMessageContract.Effect.MessageError -> {
                    onError?.invoke(effect.error)
                }
            }
        }.collect()
    }

    Column(Modifier.background(MaterialTheme.colorScheme.background)) {
        HorizontalDivider(
            color = Color.Gray,
            modifier = Modifier
                .padding(bottom = 8.dp)
                .fillMaxWidth(),
            thickness = 0.5.dp,
        )
        TextField(
            modifier = Modifier.fillMaxWidth(),
            colors = TextFieldDefaults.colors().copy(
                unfocusedContainerColor = Color.Transparent,
                focusedContainerColor = Color.Transparent,
                errorContainerColor = Color.Transparent,
            ),
            value = viewModel.viewState.value.message,
            placeholder = { Text(stringResource(Res.string.chatroom_input_message)) },
            onValueChange = { viewModel.setState { copy(message = it) } },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Send,
            ),
            keyboardActions = KeyboardActions(
                onSend = {
                    if (viewModel.viewState.value.message.trim().isEmpty()) return@KeyboardActions
                    keyboardController?.hide()
                    viewModel.setEvent(SendMessageContract.Event.SendMessage)
                },
            ),
            enabled = !viewModel.viewState.value.isLoading,
            trailingIcon = {
                if (viewModel.viewState.value.isLoading) {
                    CircularProgressIndicator(
                        color = MaterialTheme.colorScheme.primary,
                    )
                } else {
                    IconButton(
                        enabled = viewModel.viewState.value.message.isNotEmpty(),
                        onClick = {
                            if (viewModel.viewState.value.message.trim().isEmpty()) return@IconButton
                            keyboardController?.hide()
                            viewModel.setEvent(SendMessageContract.Event.SendMessage)
                        },
                    ) {
                        Icon(
                            imageVector = Icons.AutoMirrored.Filled.Send,
                            contentDescription = "Send Message",
                        )
                    }
                }
            },
        )
    }
}
