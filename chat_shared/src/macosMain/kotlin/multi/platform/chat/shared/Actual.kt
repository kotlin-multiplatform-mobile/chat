package multi.platform.chat.shared

import com.squareup.wire.GrpcClient
import multi.platform.chat.shared.app.chatroom.ChatRoomViewModel
import multi.platform.chat.shared.app.chatroom.listmessages.ListMessagesViewModel
import multi.platform.chat.shared.app.chatroom.listusers.ListUsersViewModel
import multi.platform.chat.shared.app.chatroom.sendmessage.SendMessageViewModel
import multi.platform.chat.shared.data.chat.ChatRepositoryImpl
import multi.platform.chat.shared.domain.chat.ChatRepository
import multi.platform.chat.shared.domain.chat.usecase.GetMessagesLocalUseCase
import multi.platform.chat.shared.domain.chat.usecase.GrpcFirestoreListenUseCase
import multi.platform.chat.shared.domain.chat.usecase.GrpcOnlineUsersUseCase
import multi.platform.chat.shared.domain.chat.usecase.GrpcSendMessageUseCase
import multi.platform.chat.shared.domain.chat.usecase.SetMessagesLocalUseCase
import multi.platform.chat.shared.external.ChatConfig
import multi.platform.chat.shared.external.ktor.KtorGrpcClient
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import user.BroadcastClient
import user.GrpcBroadcastClient

actual fun chatModule(chatConfig: ChatConfig) = module {
    single<ChatRepository> { ChatRepositoryImpl(get(), get(), get()) }
    single<GrpcClient> { KtorGrpcClient(get(), get()) }
    single<BroadcastClient> { GrpcBroadcastClient(get()) }

    singleOf(::GetMessagesLocalUseCase)
    singleOf(::SetMessagesLocalUseCase)
    singleOf(::GrpcSendMessageUseCase)
    singleOf(::GrpcFirestoreListenUseCase)
    singleOf(::GrpcOnlineUsersUseCase)

    factoryOf(::ChatRoomViewModel)
    factoryOf(::ListMessagesViewModel)
    factoryOf(::ListUsersViewModel)
    factoryOf(::SendMessageViewModel)
}
