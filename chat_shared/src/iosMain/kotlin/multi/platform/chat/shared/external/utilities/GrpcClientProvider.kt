package multi.platform.chat.shared.external.utilities

import com.squareup.wire.GrpcCall
import com.squareup.wire.GrpcClient
import com.squareup.wire.GrpcMethod
import com.squareup.wire.GrpcStreamingCall

class GrpcClientProvider(val baseUrl: String) : GrpcClient() {
    override fun <S : Any, R : Any> newCall(method: GrpcMethod<S, R>): GrpcCall<S, R> {
        TODO("Not yet implemented")
//        val configuration = NSURLSessionConfiguration.defaultSessionConfiguration
//        val session = NSURLSession.sessionWithConfiguration(configuration)
//
//        val url = NSURL(baseUrl)
//        val urlRequest = NSMutableURLRequest.requestWithURL(url)
//        urlRequest.setHTTPMethod("POST")
//        urlRequest.setHTTPBody(NSData.dataWithContentsOfFile("test.json"))
//        session.dataTaskWithRequest(urlRequest) { data, response, error ->
//            println(data)
//        }.resume()
//        return S to R
    }

    override fun <S : Any, R : Any> newStreamingCall(method: GrpcMethod<S, R>): GrpcStreamingCall<S, R> {
        TODO("Not yet implemented")
    }
}
