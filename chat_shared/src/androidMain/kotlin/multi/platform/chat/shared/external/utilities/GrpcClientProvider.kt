package multi.platform.chat.shared.external.utilities

import android.os.Build
import com.squareup.wire.GrpcClient
import okhttp3.OkHttpClient
import okhttp3.Protocol.HTTP_1_1
import okhttp3.Protocol.HTTP_2
import java.time.Duration

object GrpcClientProvider {
    operator fun invoke(baseUrl: String, minCompress: Long = Long.MAX_VALUE) = GrpcClient.Builder()
        .minMessageToCompress(minCompress)
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .build()

    private val okHttpClient = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        OkHttpClient.Builder()
            .readTimeout(Duration.ofMinutes(1))
            .writeTimeout(Duration.ofMinutes(1))
            .callTimeout(Duration.ofMinutes(1))
            .protocols(listOf(HTTP_1_1, HTTP_2))
            .build()
    } else {
        OkHttpClient.Builder()
            .protocols(listOf(HTTP_1_1, HTTP_2))
            .build()
    }
}
