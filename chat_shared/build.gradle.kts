@file:Suppress("UNUSED_VARIABLE", "UnstableApiUsage")

import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import com.fasterxml.jackson.module.kotlin.jsonMapper
import groovy.util.Node
import groovy.util.NodeList
import org.jetbrains.kotlin.gradle.plugin.mpp.apple.XCFrameworkConfig

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.compose)
    alias(libs.plugins.kover)
    alias(libs.plugins.realm)
    alias(libs.plugins.wire)
    kotlin("native.cocoapods")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-parcelize")
    id("kotlinx-serialization")
    `maven-publish`
}

val repoId: String by project
val appId: String by project
val appName: String by project
val appVersionName: String by project
val androidCompileSdkVersion: String by project
val androidMinSdkVersion: String by project
val iosDeploymentTarget: String by project
val osxDeploymentTarget: String by project
val localProperties = gradleLocalProperties(rootDir)

group = appId
version = appVersionName

android {
    namespace = "$appId.shared"
    compileSdk = androidCompileSdkVersion.toInt()
    defaultConfig {
        minSdk = androidMinSdkVersion.toInt()

        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            setProguardFiles(mutableListOf("proguard-rules.pro"))
        }
        getByName("debug") {
            isMinifyEnabled = false
            enableUnitTestCoverage = true
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    buildFeatures {
        dataBinding = true
        buildConfig = true
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.compose.compiler.get()
    }
    kotlin {
        jvmToolchain(JavaVersion.VERSION_17.toString().toInt())
    }
}

kotlin {
    androidTarget {
        compilations.all {
            kotlinOptions {
                jvmTarget = JavaVersion.VERSION_17.toString()
            }
        }
        publishAllLibraryVariants()
        publishLibraryVariantsGroupedByFlavor = false
    }
    iosX64()
    iosArm64()
    iosSimulatorArm64()

    macosX64()
    macosArm64()
    jvm("desktop")
    /* currently realm not supported on js
    js(IR) {
        browser()
    }
    */

    val xcf = XCFrameworkConfig(project)
    cocoapods {
        summary = "Provide Sign In with multiple provider"
        homepage = "https://gitlab.com/kotlin-multiplatform-mobile/chat"
        version = appVersionName
        ios.deploymentTarget = iosDeploymentTarget
        osx.deploymentTarget = osxDeploymentTarget
        framework {
            isStatic = false
            xcf.add(this)
        }
        podfile = project.file("../example_ios/Podfile")
        pod("Wire")
    }

    sourceSets {
        all {
            languageSettings {
                optIn("org.jetbrains.compose.resources.ExperimentalResourceApi")
            }
        }
        val commonMain by getting {
            dependencies {
                api(libs.kmm.core)
                api(libs.wire.runtime)
                api(libs.wire.grpc.client)
                implementation(compose.components.resources)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-annotations-common"))
                implementation(libs.coroutines.test)
                implementation(libs.mockk.common)
            }
        }
        val androidMain by getting {
            dependsOn(commonMain)
            dependencies {}
            resources.srcDir("./res")
        }
        val androidUnitTest by getting {
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation(libs.junit)
                implementation(libs.mockk)
            }
        }

        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
        }

        val desktopMain by getting

        val macosX64Main by getting
        val macosArm64Main by getting
        val macosMain by creating {
            dependsOn(commonMain)
            macosX64Main.dependsOn(this)
            macosArm64Main.dependsOn(this)
        }

        /* currently realm not supported on js
        val jsMain by getting {
            dependsOn(commonMain)
        }
        */
    }

    task("testClasses")
}

publishing {
    publications {
        create<MavenPublication>("release") {
            from(components["kotlin"])
            pom.withXml {
                val dependencies =
                    ((asNode()["dependencies"] as NodeList)[0] as Node).value() as NodeList
                if (dependencies.isNotEmpty()) {
                    dependencies.map { it as Node }.forEach {
                        val scope = (it["scope"] as NodeList)[0] as Node
                        scope.setValue("compile")
                    }
                }
            }
        }
    }
    repositories {
        maven {
            name = appName
            url = uri("https://gitlab.com/api/v4/projects/${repoId}/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = localProperties.getProperty("token")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

koverReport {
    val excs: MutableList<String> by rootProject.extra
    defaults {
        mergeWith("debug")
        verify {
            rule("Minimal line coverage rate in percents") {
                minBound(0)
            }
        }
    }
    filters {
        excludes {
            classes(*excs.toTypedArray())
        }
    }
    androidReports("release") {
        filters {
            excludes {
                classes(*excs.toTypedArray())
            }
        }
    }
}

wire {
    kotlin {
        rpcRole = "client"
        rpcCallStyle = "suspending"
        javaInterop = true
    }
}